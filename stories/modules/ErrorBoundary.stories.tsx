/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Button, ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import { ErrorBoundary } from '../../src/components/ErrorBoundary';

export default { title: 'modules/Error Boundary' };

const ErrorTest: React.FC<{}> = () => {
  const [hasError, triggerError] = React.useState(false);

  if (hasError) {
    throw new Error('Triggered error');
  }
  return (
    <Button
      variant="outlined"
      color="secondary"
      onClick={(): void => triggerError(true)}
    >
      Click to trigger error
    </Button>
  );
};

export const ErrorBoundaryDemo = (): React.ReactElement => (
  <ThemeProvider theme={GWTheme}>
    <ErrorBoundary
      onError={(error: Error, errorInfo: React.ErrorInfo): void =>
        // eslint-disable-next-line no-console
        console.log('send error to service:', error, errorInfo)
      }
    >
      <ErrorTest />
    </ErrorBoundary>
  </ThemeProvider>
);
