/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { ThemeProvider, Grid } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import {
  Notifications,
  NotificationRow,
} from '../../src/components/Notifications';
import { NotificationTriggerProvider } from '../../src/components/NotificationTrigger';
import {
  ApiProvider,
  useApiContext,
} from '../../src/components/ApiContext/ApiContext';
import { SW_BASE_URL } from '../storybookUtils';

export default { title: 'modules/Notification Section' };

const fakeEvent = {
  eventid: 'METRB3',
  category: 'ELECTRON_FLUX',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      owner: 'METOffice',
      threshold: '',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'ended',
      canbe: [],
    },
    internalprovider: {
      eventid: 'METRB3',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      threshold: '',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'ended',
      canbe: [],
    },
  },
};

const fakeEventWithBell = {
  eventid: 'METRB3',
  category: 'XRAY_RADIO_BLACKOUT',
  originator: 'METOffice',
  notacknowledged: true,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      owner: 'METOffice',
      threshold: '',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
  },
};

const fakeEventWithDraft = {
  eventid: 'METRB3',
  category: 'XRAY_RADIO_BLACKOUT',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      owner: 'METOffice',
      threshold: '',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
    internalprovider: {
      draft: true,
      eventid: 'METRB3',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      threshold: '',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'draft',
      canbe: [],
    },
  },
};

const NotificationsDemoDummyActionsComponent = (): React.ReactElement => {
  const { api } = useApiContext();
  return (
    <Grid container>
      <Grid item xs={6}>
        <NotificationTriggerProvider>
          <Notifications
            requestPromiseStore={api.fakeIssueNotification}
            requestPromiseDiscardDraft={api.fakeDiscardDraftNotification}
            requestPromiseAcknowledge={api.fakeSetAcknowledged}
          />
        </NotificationTriggerProvider>
      </Grid>
    </Grid>
  );
};

export const NotificationsDemoDummyActions = (): React.ReactElement => {
  return (
    <ThemeProvider theme={GWTheme}>
      <ApiProvider baseURL={SW_BASE_URL}>
        <NotificationsDemoDummyActionsComponent />
      </ApiProvider>
    </ThemeProvider>
  );
};

export const NotificationRowDummyDemo = (): React.ReactElement => {
  return (
    <ThemeProvider theme={GWTheme}>
      <NotificationRow
        event={fakeEvent}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello');
        }}
      />
      <NotificationRow
        event={fakeEventWithBell}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello bell');
        }}
      />
      <NotificationRow
        event={fakeEventWithDraft}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello draft');
        }}
      />
    </ThemeProvider>
  );
};
