/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import { LifeCycleDialog } from '../../src/components/LifeCycleDialog';
import {
  ApiProvider,
  useApiContext,
} from '../../src/components/ApiContext/ApiContext';
import { SW_BASE_URL } from '../storybookUtils';

export default { title: 'modules/LifeCycle Section' };

interface WrapperProps {
  children: React.ReactNode;
}

const Wrapper: React.FC<WrapperProps> = ({ children }: WrapperProps) => (
  <ThemeProvider theme={GWTheme}>
    <ApiProvider baseURL={SW_BASE_URL}>{children}</ApiProvider>
  </ThemeProvider>
);

const NewNotificationComponent = (): React.ReactElement => {
  const { api } = useApiContext();
  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="new"
      requestPromiseStore={api.fakeIssueNotification}
      requestPromiseDiscardDraft={api.fakeDiscardDraftNotification}
    />
  );
};

export const NewNotification = (): React.ReactElement => {
  return (
    <Wrapper>
      <NewNotificationComponent />
    </Wrapper>
  );
};

const MetOfficeDummyEventComponent = (): React.ReactElement => {
  const { api } = useApiContext();

  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="METRB1"
      event={{
        eventid: 'METRB1',
        category: 'XRAY_RADIO_BLACKOUT',
        originator: 'METOffice',

        lifecycles: {
          externalprovider: {
            eventid: 'METRB1',
            label: 'ALERT',
            eventlevel: 'R1',
            owner: 'METOffice',
            firstissuetime: '2020-07-13 12:00',
            lastissuetime: '2020-07-13 12:00',
            eventstart: '2020-07-13 13:00',
            eventend: '',
            threshold: '1.0 x 10^3',
            state: 'issued',
            canbe: ['updated', 'extended', 'summarised'],
          },
          internalprovider: {
            eventid: 'METRB1',
            label: 'ALERT',
            owner: 'KNMI',
            firstissuetime: '2020-07-13 12:10',
            lastissuetime: '2020-07-13 14:58',
            state: 'issued',
            eventlevel: 'R1',
            eventstart: '2020-07-13 13:00',
            eventend: '',
            threshold: '1.0 x 10^3',
            canbe: ['updated', 'extended', 'summarised'],
          },
        },
      }}
      requestPromiseEvent={api.getFakeEvent}
      requestPromiseStore={api.fakeIssueNotification}
      requestPromiseDiscardDraft={api.fakeDiscardDraftNotification}
    />
  );
};

export const MetOfficeDummyEvent = (): React.ReactElement => {
  return (
    <Wrapper>
      <MetOfficeDummyEventComponent />
    </Wrapper>
  );
};

const MetOfficeSummarisedDummyEventComponent = (): React.ReactElement => {
  const { api } = useApiContext();

  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="METRB3"
      event={{
        eventid: 'METRB3',
        category: 'ELECTRON_FLUX',
        originator: 'METOffice',
        lifecycles: {
          externalprovider: {
            eventid: 'METRB3',
            eventstart: '2020-07-13 13:00',
            eventend: '2020-07-13 16:12',
            label: 'ALERT',
            owner: 'METOffice',
            firstissuetime: '2020-07-09 06:13',
            lastissuetime: '2020-07-09 06:13',
            state: 'ended',
            threshold: '',
            canbe: [],
          },
          internalprovider: {
            eventid: 'METRB3',
            eventstart: '2020-07-13 13:00',
            eventend: '2020-07-13 16:12',
            label: 'ALERT',
            owner: 'KNMI',
            firstissuetime: '2020-07-09 06:16',
            threshold: '',
            lastissuetime: '2020-07-09 06:16',
            state: 'ended',
            canbe: [],
          },
        },
      }}
      requestPromiseEvent={api.getFakeEvent}
      requestPromiseStore={api.fakeIssueNotification}
      requestPromiseDiscardDraft={api.fakeDiscardDraftNotification}
    />
  );
};

export const MetOfficeSummarisedDummyEvent = (): React.ReactElement => {
  return (
    <Wrapper>
      <MetOfficeSummarisedDummyEventComponent />
    </Wrapper>
  );
};
