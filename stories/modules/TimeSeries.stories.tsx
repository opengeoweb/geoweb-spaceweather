/* eslint-disable react/no-array-index-key */
/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/camelcase */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import TimeSeries from '../../src/components/TimeSeries/TimeSeries';
import { TimeTrackerProvider } from '../../src/components/TimeSeries/TimeTrackerContext';
import { GraphContainer } from '../../src/components/TimeSeries/GraphContainer';
import {
  barGraphWithSeries,
  areaGraphWithSeries,
  logGraphWithSeries,
  bandGraphWithSeries,
} from '../../src/utils/DummyData';
import Tracker from '../../src/components/TimeSeries/Tracker';
import { defaultTimeRange } from '../../src/utils/defaultTimeRange';
import { ApiProvider } from '../../src/components/ApiContext/ApiContext';
import { SW_BASE_URL } from '../storybookUtils';

export default { title: 'modules/TimeSeries' };

export const TimeSeriesRealData: React.FC<{}> = () => (
  <ThemeProvider theme={GWTheme}>
    <ApiProvider baseURL={SW_BASE_URL}>
      <TimeSeries />
    </ApiProvider>
  </ThemeProvider>
);

export const BarGraphDummyData: React.FC<{}> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <ThemeProvider theme={GWTheme}>
      <ApiProvider baseURL={SW_BASE_URL}>
        <TimeTrackerProvider>
          <Tracker graphs={[barGraphWithSeries]} />
          <GraphContainer
            graphs={[barGraphWithSeries]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerProvider>
      </ApiProvider>
    </ThemeProvider>
  );
};

export const AreaGraphDummyData: React.FC<{}> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <ThemeProvider theme={GWTheme}>
      <ApiProvider baseURL={SW_BASE_URL}>
        <TimeTrackerProvider>
          <Tracker graphs={[areaGraphWithSeries]} />
          <GraphContainer
            graphs={[areaGraphWithSeries]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerProvider>
      </ApiProvider>
    </ThemeProvider>
  );
};

export const LogarithmicGraphDummyData: React.FC<{}> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <ThemeProvider theme={GWTheme}>
      <ApiProvider baseURL={SW_BASE_URL}>
        <TimeTrackerProvider>
          <Tracker graphs={[logGraphWithSeries]} />
          <GraphContainer
            graphs={[logGraphWithSeries]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerProvider>
      </ApiProvider>
    </ThemeProvider>
  );
};

export const BandGraphDummyData: React.FC<{}> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <ThemeProvider theme={GWTheme}>
      <ApiProvider baseURL={SW_BASE_URL}>
        <TimeTrackerProvider>
          <Tracker graphs={[bandGraphWithSeries]} />
          <GraphContainer
            graphs={[bandGraphWithSeries]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerProvider>
      </ApiProvider>
    </ThemeProvider>
  );
};
