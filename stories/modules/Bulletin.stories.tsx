/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import Bulletin from '../../src/components/Bulletin/Bulletin';
import BulletinHistoryDialog from '../../src/components/Bulletin/BulletinHistoryDialog';

import { ApiProvider } from '../../src/components/ApiContext/ApiContext';
import { SW_BASE_URL } from '../storybookUtils';

export default { title: 'modules/Bulletin Section' };

export const BulletinStory = (): React.ReactElement => (
  <ThemeProvider theme={GWTheme}>
    <ApiProvider baseURL={SW_BASE_URL}>
      <div style={{ height: '500px', width: '800px', display: 'flex' }}>
        <Bulletin />
      </div>
    </ApiProvider>
  </ThemeProvider>
);

export const BulletinHistoryStory = (): React.ReactElement => (
  <ThemeProvider theme={GWTheme}>
    <ApiProvider baseURL={SW_BASE_URL}>
      <div style={{ height: '500px', width: '800px', display: 'flex' }}>
        <BulletinHistoryDialog
          open
          toggleStatus={(): void => {
            /* Do nothing */
          }}
        />
      </div>
    </ApiProvider>
  </ThemeProvider>
);
