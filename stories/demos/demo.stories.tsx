/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  ThemeProvider,
  Paper,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import { LayoutTitle } from '../../src';
import Layout from '../../src/components/Layout/Layout';

import { Bulletin } from '../../src/components/Bulletin';
import { Notifications } from '../../src/components/Notifications';
import {
  NotificationTrigger,
  NotificationTriggerProvider,
} from '../../src/components/NotificationTrigger';
import { ErrorBoundary } from '../../src/components/ErrorBoundary';
import TimeSeries from '../../src/components/TimeSeries/TimeSeries';
import { ApiProvider } from '../../src/components/ApiContext/ApiContext';
import { SW_BASE_URL } from '../storybookUtils';

export default { title: 'demo' };

const useStyles1 = makeStyles(() => ({
  Dashboard: {
    maxHeight: '95vh',
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'hidden',
  },
  DashboardBody: { overflowY: 'scroll', paddingBottom: '10px' },
}));

export const SpaceWeatherDemo = (): React.ReactElement => {
  const classes = useStyles1();
  return (
    <ThemeProvider theme={GWTheme}>
      <ErrorBoundary>
        <ApiProvider baseURL={SW_BASE_URL}>
          <NotificationTriggerProvider>
            <div className={classes.Dashboard}>
              <LayoutTitle />
              <NotificationTrigger />
              <div className={classes.DashboardBody}>
                <Layout
                  leftTop={<Bulletin />}
                  leftBottom={<Notifications />}
                  right={<TimeSeries />}
                />
              </div>
            </div>
          </NotificationTriggerProvider>
        </ApiProvider>
      </ErrorBoundary>
    </ThemeProvider>
  );
};

const useStyles2 = makeStyles(theme => ({
  Dialog: {
    position: 'absolute',
    top: '2%',
    left: '5%',
    maxWidth: '90%',
    maxHeight: '95%',
    display: 'flex',
    flexDirection: 'column',
    zIndex: 550,
    overflowY: 'hidden',
  },
  DialogHeader: {
    height: '30px',
    padding: theme.spacing(1),
    boxShadow:
      '0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
    textAlign: 'center',
  },
  dialogBody: { overflowY: 'scroll', paddingBottom: '10px' },
}));

export const SpaceWeatherDialogDemo = (): React.ReactElement => {
  const classes = useStyles2();
  return (
    <ThemeProvider theme={GWTheme}>
      <ErrorBoundary>
        <ApiProvider baseURL={SW_BASE_URL}>
          <NotificationTriggerProvider>
            <Paper data-testid="spaceWeatherDialog" className={classes.Dialog}>
              <div className={classes.DialogHeader}>
                <Typography variant="subtitle1">
                  Space Weather Forecast
                </Typography>
              </div>
              <div>
                <NotificationTrigger />
              </div>
              <div className={classes.dialogBody}>
                <Layout
                  leftTop={<Bulletin />}
                  leftBottom={<Notifications />}
                  right={<TimeSeries />}
                />
              </div>
            </Paper>
          </NotificationTriggerProvider>
        </ApiProvider>
      </ErrorBoundary>
    </ThemeProvider>
  );
};
