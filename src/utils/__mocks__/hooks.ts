/* eslint-disable @typescript-eslint/camelcase */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import { ApiHookProps } from '../hooks';

export const mockBulletin = {
  bulletin_id: '5f0ef3eff5b3da68c0d1e3c2',
  id: '5f0ef3eff5b3da68c0d1e3c2',
  message: {
    categories: [
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'ELECTRON',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'GEOMAG',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
        timestamp: '2020-07-15T12:17:46',
        type: 'PROTON',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'XRAY',
      },
    ],
    overview: {
      content: 'This is some fake bulletin content',
      simplified_content: 'This is some fake bulletin content',
      timestamp: '2020-07-15T12:17:46',
      type: 'ISSUED',
    },
    status: 'ISSUED',
  },
  time: 1594815466,
};

export const mockBulletinResult = {
  isLoading: false,
  error: null,
  result: {
    message: {
      overview: {
        content: 'Technical content',
        // eslint-disable-next-line @typescript-eslint/camelcase
        simplified_content: 'Simplified content',
        timestamp: '2020-07-14T12:11:46',
      },
    },
  },
};

export const mockBulletinResultIncomplete = {
  isLoading: false,
  error: null,
  result: {
    message: { overview: {} },
  },
};

export const mockLoading = {
  isLoading: true,
  error: null,
  result: null,
};

export const mockError = {
  isLoading: false,
  error: new Error('Mocking error message'),
  result: null,
};

export const mockBulletinHistory = {
  isLoading: false,
  error: null,
  fetchApiData: jest.fn(),
  result: [
    {
      bulletin_id: '45345345345',
      id: '45345345345',
      message: {
        categories: [
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'ELECTRON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'GEOMAG',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
            timestamp: '2020-07-15T12:17:46',
            type: 'PROTON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'XRAY',
          },
        ],
        overview: {
          content: 'This is some fake bulletin content',
          simplified_content: 'This is some fake bulletin content',
          timestamp: '2020-07-15T12:17:46',
          type: 'ISSUED',
        },
        status: 'ISSUED',
      },
      time: 1594815466,
    },
    {
      bulletin_id: '5f0ef3eff5b3da68c0d1e3c2',
      id: '5f0ef3eff5b3da68c0d1e3c2',
      message: {
        categories: [
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'ELECTRON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'GEOMAG',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
            timestamp: '2020-07-14T12:17:46',
            type: 'PROTON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'XRAY',
          },
        ],
        overview: {
          content: 'This is some fake bulletin content2',
          simplified_content: 'This is some fake bulletin content2',
          timestamp: '2020-07-14T12:17:46',
          type: 'ISSUED',
        },
        status: 'ISSUED',
      },
      time: 1594815466,
    },
  ],
};

export const mockEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  originator: 'KNMI',
  notacknowledged: true,

  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: '2020-07-14T11:20:00Z',
      eventend: '2020-07-14T12:55:00Z',
      threshold: '',
      eventlevel: 'G1',
      firstissuetime: '2020-07-14T11:13:00Z',
      lastissuetime: '2020-07-14T11:13:00Z',
      state: 'issued',

      canbe: ['updated', 'extended', 'summarised'],
      notifications: [
        {
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          neweventlevel: 'G1',
          threshold: '',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: '2020-07-14T11:20:00Z',
          neweventend: '2020-07-14T12:55:00Z',
          message:
            '<b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB2MET_8373489',
          srcEventId: 'METRB2MET_8373489',
          issuetime: '2020-07-14T11:13:00Z',
        },
      ],
    },
  },
};

export const mockEventUnacknowledgedExternal = {
  eventid: 'METRB3',
  category: 'GEOMAGNETIC',
  originator: 'MetOffice',
  notacknowledged: true,

  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      label: 'ALERT',
      owner: 'MetOffice',
      eventstart: '2020-07-14T11:20:00Z',
      eventend: '2020-07-14T12:55:00Z',
      threshold: '',
      eventlevel: 'G1',
      firstissuetime: '2020-07-14T11:13:00Z',
      lastissuetime: '2020-07-14T11:13:00Z',
      state: 'issued',

      canbe: ['updated', 'extended', 'summarised'],
      notifications: [
        {
          eventid: 'METRB3',
          category: 'GEOMAGNETIC',
          neweventlevel: 'G1',
          threshold: '',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: '2020-07-14T11:20:00Z',
          neweventend: '2020-07-14T12:55:00Z',
          message: 'Here is this jolly old test message',
          notificationid: 'METRB2MET_8373489',
          srcEventId: 'METRB2MET_8373489',
          issuetime: '2020-07-14T11:13:00Z',
        },
      ],
    },
  },
};

export const mockEventAcknowledgedExternal = {
  eventid: 'METRB1123454',
  category: 'PROTON_FLUX',
  originator: 'METOffice',

  lifecycles: {
    externalprovider: {
      eventid: 'METRB1123454',
      label: 'ALERT',
      owner: 'METOffice',
      firstissuetime: '2020-07-12 03:22',
      lastissuetime: '2020-07-12 03:22',
      threshold: '1.0 x 10^3',
      eventlevel: 'S2',
      state: 'issued',
      eventstart: '2020-07-12 04:00',
      eventend: '2020-07-12 06:00',
      canbe: [],
      notifications: [
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          label: 'ALERT',
          changestateto: 'issued',
          neweventlevel: 'S2',
          threshold: '1.0 x 10^3',
          neweventstart: '2020-07-12 04:00',
          neweventend: '2020-07-12 06:00',
          message:
            '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB1123454',
          srcEventId: 'METRB1123454',
          issuetime: '2020-07-12 03:22',
        },
      ],
    },
    internalprovider: {
      eventid: 'METRB1123454',
      label: 'WARNING',
      owner: 'KNMI',
      state: 'issued',
      eventstart: '2020-07-12 04:00',
      eventend: '2020-07-12 06:00',
      firstissuetime: '2020-07-12 03:30',
      lastissuetime: '2020-07-12 03:50',
      threshold: '1.0 x 10^3',
      draft: false,
      canbe: ['updated', 'extended', 'cancelled'],
      notifications: [
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          neweventlevel: 'S2',
          label: 'WARNING',
          changestateto: 'issued',
          draft: false,
          neweventstart: '2020-07-12 04:20',
          neweventend: '2020-07-12 06:20',
          threshold: '1.0 x 10^3',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          srcEventId: 'METRB1123454',
          issuetime: '2020-07-12 03:30',
        },
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          neweventlevel: 'S2',
          label: 'WARNING',
          changestateto: 'issued',
          draft: false,
          neweventstart: '2020-07-12 04:20',
          neweventend: '2020-07-12 06:20',
          threshold: '1.0 x 10^3',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          srcEventId: 'METRB1123454',
          issuetime: '2020-07-12 03:50',
        },
      ],
    },
  },
};

export const mockEventAcknowledgedExternalDraft = {
  eventid: 'METRB1123453453454',
  category: 'PROTON_FLUX',
  originator: 'METOffice',

  lifecycles: {
    externalprovider: {
      eventid: 'METRB1123453453454',
      label: 'ALERT',
      owner: 'METOffice',
      firstissuetime: '2020-07-12 03:22',
      lastissuetime: '2020-07-12 03:22',
      threshold: '1.0 x 10^3',
      eventlevel: 'S2',
      state: 'issued',
      eventstart: '2020-07-12 04:00',
      eventend: '2020-07-12 06:00',
      canbe: [],
      notifications: [
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          label: 'ALERT',
          changestateto: 'issued',
          neweventlevel: 'S2',
          threshold: '1.0 x 10^3',
          neweventstart: '2020-07-12 04:00',
          neweventend: '2020-07-12 06:00',
          message:
            '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB1123453453454',
          srcEventId: 'METRB1123453453454',
          issuetime: '2020-07-12 03:22',
        },
      ],
    },
    internalprovider: {
      eventid: 'METRB1123453453454',
      label: 'WARNING',
      owner: 'KNMI',
      state: 'issued',
      eventstart: '2020-07-12 04:00',
      eventend: '2020-07-12 06:00',
      firstissuetime: '2020-07-12 03:30',
      lastissuetime: '2020-07-12 03:50',
      threshold: '1.0 x 10^3',
      draft: true,
      canbe: ['updated', 'extended', 'summarised'],
      notifications: [
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          neweventlevel: 'S2',
          label: 'WARNING',
          changestateto: 'issued',
          draft: false,
          neweventstart: '2020-07-12 04:20',
          neweventend: '2020-07-12 06:20',
          threshold: '1.0 x 10^3',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          srcEventId: 'METRB1123453453454',
          issuetime: '2020-07-12 03:30',
        },
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          neweventlevel: 'S2',
          label: 'WARNING',
          changestateto: 'issued',
          draft: false,
          neweventstart: '2020-07-12 04:20',
          neweventend: '2020-07-12 06:20',
          threshold: '1.0 x 10^3',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION1224455655',
          srcEventId: 'METRB1123453453454',
          issuetime: '2020-07-12 03:35',
        },
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          neweventlevel: 'S2',
          label: 'WARNING',
          changestateto: 'issued',
          draft: true,
          neweventstart: '2020-07-12 04:20',
          neweventend: '2020-07-12 06:20',
          threshold: '1.0 x 10^3',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          srcEventId: 'METRB1123453453454',
          issuetime: '2020-07-12 03:50',
        },
      ],
    },
  },
};

export const mockEventResult = {
  isLoading: false,
  error: null,
  result: mockEvent,
};

export const mockEventListResult = {
  isLoading: false,
  error: null,
  result: [
    mockEventAcknowledgedExternal,
    mockEvent,
    mockEventUnacknowledgedExternal,
    mockEventAcknowledgedExternalDraft,
  ],
  fetchApiData: jest.fn(),
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useApi = (apiCall, params): ApiHookProps => {
  switch (apiCall.name) {
    case 'getBulletin':
      if (params === 'result') {
        return mockBulletinResult;
      }
      if (params === 'incomplete') {
        return mockBulletinResultIncomplete;
      }
      if (params === 'loading') {
        return mockLoading;
      }
      if (params === 'error') {
        return mockError;
      }
      if (params) {
        const filteredResult = mockBulletinHistory.result.filter(
          bulletin => bulletin.bulletin_id === params,
        )[0];
        return { isLoading: false, error: null, result: filteredResult };
      }
      return mockBulletinResult;

    case 'getBulletinHistory': {
      return mockBulletinHistory;
    }

    case 'getEvent': {
      if (params) {
        const result = mockEventListResult.result.find(
          event => event.eventid === params,
        );
        return {
          isLoading: false,
          error: null,
          result,
        };
      }
      return {
        isLoading: false,
        error: null,
        result: mockEvent,
      };
    }

    case 'getEventList': {
      if (params === 'loading') {
        return mockLoading;
      }
      if (params === 'error') {
        return mockError;
      }
      if (params) {
        const newresult = {
          isLoading: false,
          error: null,
          result: mockEventListResult.result,
          fetchApiData: jest.fn(),
        };
        if (params.category) {
          newresult.result = mockEventListResult.result.filter(
            event => event.category === params.category,
          );
        } else if (params.originator && params.originator === 'KNMI') {
          newresult.result = mockEventListResult.result.filter(
            event => event.originator === 'KNMI',
          );
        }
        return newresult;
      }
      return mockEventListResult;
    }

    case 'success': {
      return mockBulletinResult;
    }

    case 'error': {
      return mockError;
    }

    case 'loading':
    default: {
      return mockLoading;
    }
  }
};
