/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import {
  kpIndexDummyData,
  kpIndexForecastDummyData,
  interplanetaryMagneticFieldBt,
  solarWindSpeedDummyData,
  solarWindPressureDummyData,
  solarWindDensityDummyData,
  xRayFluxDummyData,
} from '../DummyData';
import { TimeseriesParams, TimeseriesResponseData } from '../../types';

export const cancelRequestById = jest.fn();
export const createCancelRequestId = jest.fn();
export const api = {
  issueNotification: (formData): Promise<{ data: boolean }> => {
    if (formData.draft) {
      return new Promise(resolve => {
        resolve({ data: true });
      });
    }
    return Promise.reject(
      new Error('Mocking a failure for issue notification'),
    );
  },
  discardDraftNotification: (): Promise<{ data: string }> => {
    return new Promise(resolve => {
      resolve({ data: 'All good' });
    });
  },
  getTimeSeriesMultiple: (
    params: TimeseriesParams[],
  ): Promise<{ data: TimeseriesResponseData }[]> => {
    const bundeledSeries = params.map(param => {
      const getDummyTimeSerie = (
        stream: string,
        parameter: string,
      ): TimeseriesResponseData => {
        switch (stream) {
          case 'kp':
            return kpIndexDummyData;
          case 'kpforecast':
            return kpIndexForecastDummyData;
          case 'rtsw_mag':
            return parameter === 'bt'
              ? interplanetaryMagneticFieldBt
              : interplanetaryMagneticFieldBt;
          case 'rtsw_wind': {
            if (param.parameter === 'proton_speed') {
              return solarWindSpeedDummyData;
            }
            if (param.parameter === 'proton_pressure') {
              return solarWindPressureDummyData;
            }
            if (param.parameter === 'proton_density') {
              return solarWindDensityDummyData;
            }
            return solarWindDensityDummyData;
          }
          case 'xray':
            return xRayFluxDummyData;
          default:
            return kpIndexDummyData;
        }
      };

      return {
        data: getDummyTimeSerie(param.stream, param.parameter),
      };
    });
    return Promise.all(bundeledSeries);
  },
};

export const mockDraftEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  originator: 'KNMI',
  notacknowledged: true,
  eventlevel: 'G1',
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: '2020-07-14 11:20',
      eventend: '2020-07-14 12:55',
      threshold: '',
      firstissuetime: '2020-07-14 11:13',
      lastissuetime: '2020-07-14 11:13',
      state: 'issued',

      canbe: ['updated', 'extended', 'summarised'],
      notifications: [
        {
          draft: true,
          changestateto: '',
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          neweventlevel: 'G1',
          threshold: '',
          label: 'ALERT',
          neweventstart: '2020-07-14 11:20',
          neweventend: '2020-07-14 12:55',
          message: '',
          notificationid: 'METRB2MET_8373489',
          srcEventId: 'METRB2MET_8373489',
          issuetime: '2020-07-14 11:13',
        },
      ],
    },
  },
};

export const createApi = (): object => api;
