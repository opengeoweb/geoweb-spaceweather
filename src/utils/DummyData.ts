/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import moment from 'moment';
import { createChartSeriesData } from '../components/TimeSeries/createChartSeriesData';
import { GraphItem } from '../components/TimeSeries/types';

export const solarWindSpeedDummyData = {
  parameter: 'proton_speed',
  stream: 'rtsw_wind',
  unit: 'km/s',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('24', 'hours')
        .format()
        .toString(),
      value: 311,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('23', 'hours')
        .format()
        .toString(),
      value: 313,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('22', 'hours')
        .format()
        .toString(),
      value: 310,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('21', 'hours')
        .format()
        .toString(),
      value: 303,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('20', 'hours')
        .format()
        .toString(),
      value: 337,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('19', 'hours')
        .utc()
        .format()
        .toString(),
      value: 324,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('18', 'hours')
        .format()
        .toString(),
      value: 320,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('17', 'hours')
        .format()
        .toString(),
      value: 326,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')

        .format()
        .toString(),
      value: 320,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('2', 'hours')
        .format()
        .toString(),
      value: 303,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('1', 'hours')
        .format()
        .toString(),
      value: 313,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 207,
      source: 'DSCOVR',
    },
  ],
};

export const solarWindPressureDummyData = {
  parameter: 'proton_pressure',
  stream: 'rtsw_wind',
  unit: 'nPa',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('16', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('8', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('4', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('2', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('1', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
  ],
};

export const solarWindDensityDummyData = {
  parameter: 'proton_density',
  stream: 'rtsw_wind',
  unit: '1/cm3',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('15', 'hours')
        .format()
        .toString(),
      value: 8,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('14', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('13', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')
        .format()
        .toString(),
      value: NaN,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('11', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('10', 'hours')
        .format()
        .toString(),
      value: 5,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('9', 'hours')
        .format()
        .toString(),
      value: 4,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('8', 'hours')
        .format()
        .toString(),
      value: 11,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('7', 'hours')
        .format()
        .toString(),
      value: 11,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('6', 'hours')
        .format()
        .toString(),
      value: 32,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('5', 'hours')
        .format()
        .toString(),
      value: 100,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('4', 'hours')
        .format()
        .toString(),
      value: 70,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('2', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('1', 'hours')
        .format()
        .toString(),
      value: 6,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 5,
      source: 'DSCOVR',
    },
  ],
};

export const kpIndexDummyData = {
  parameter: 'kp',
  stream: 'kp',
  unit: '-',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('15', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')
        .format()
        .toString(),
      value: 0.66,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .subtract('9', 'hours')
        .format()
        .toString(),
      value: 0.0,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .subtract('6', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')
        .format()
        .toString(),
      value: 0.33,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 3,
      source: 'GFZ',
    },
  ],
};

export const kpIndexForecastDummyData = {
  parameter: 'kp_24',
  stream: 'kp',
  unit: '-',
  data: [
    {
      timestamp: moment
        .utc()
        .add('3', 'hours')
        .format()
        .toString(),
      value: 4,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('6', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('9', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('12', 'hours')

        .format()
        .toString(),
      value: 7,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('15', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('18', 'hours')

        .format()
        .toString(),
      value: 1,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('21', 'hours')

        .format()
        .toString(),
      value: 3,
      source: 'GFZ',
    },
    {
      timestamp: moment
        .utc()
        .add('24', 'hours')

        .format()
        .toString(),
      value: 3,
      source: 'GFZ',
    },
  ],
};

export const xRayFluxDummyData = {
  parameter: 'solar_x_ray_flux_long',
  stream: 'xray',
  unit: 'W/m2',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')

        .format()
        .toString(),
      value: 2e-5,
      source: 'GOES-16',
    },
    {
      timestamp: moment
        .utc()
        .subtract('9', 'hours')
        .format()
        .toString(),
      value: 4.5e-1,
      source: 'GOES-16',
    },
    {
      timestamp: moment
        .utc()
        .subtract('6', 'hours')
        .format()
        .toString(),
      value: 5e-1,
      source: 'GOES-16',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')
        .format()
        .toString(),
      value: 4.3e-2,
      source: 'GOES-16',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 4.3e-2,
      source: 'GOES-16',
    },
  ],
};

export const interplanetaryMagneticFieldBt = {
  parameter: 'bt',
  stream: 'rtsw_mag',
  unit: 'nT',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('15', 'hours')
        .format()
        .toString(),
      value: 15,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')
        .format()
        .toString(),
      value: 0,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('9', 'hours')
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('6', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')
        .format()
        .toString(),
      value: 18,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 8,
      source: 'DSCOVR',
    },
  ],
};

export const interplanetaryMagneticFieldBz = {
  parameter: 'bz_gsm',
  stream: 'rtsw_mag',
  unit: 'nT',
  data: [
    {
      timestamp: moment
        .utc()
        .subtract('15', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('12', 'hours')
        .format()
        .toString(),
      value: 6,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('9', 'hours')
        .format()
        .toString(),
      value: 1,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('6', 'hours')
        .format()
        .toString(),
      value: 3,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .subtract('3', 'hours')
        .format()
        .toString(),
      value: 4,
      source: 'DSCOVR',
    },
    {
      timestamp: moment
        .utc()
        .format()
        .toString(),
      value: 2,
      source: 'DSCOVR',
    },
  ],
};

const barGraph = {
  id: '0-kp-barGraph',
  title: 'Kp Index',
  graphType: 'BAR',
  columns: ['kpIndex', 'kpIndexForecast'],
  timeWidth: '3h',
  yMinValue: 0,
  yMaxValue: 9,
  threshold: [
    { title: 'Met Office', value: 5 },
    { title: 'KNMI', value: 7 },
  ],
  params: [{ stream: 'kp', parameter: 'kp' }],
} as GraphItem;

export const barGraphWithSeries = {
  ...barGraph,
  series: createChartSeriesData(
    [{ data: kpIndexDummyData }, { data: kpIndexForecastDummyData }],
    barGraph,
  ),
};

const areaGraph = {
  id: '3-wind-areaGraph',
  title: 'Solar Wind Speed (km/s)',
  graphType: 'AREA',
  params: [{ stream: 'rtsw_wind', parameter: 'proton_speed' }],
  columns: [['lineSolarWindSpeed', 'areaSolarWindSpeed']],
  timeWidth: '1s',
  yMinValue: 200,
  yMaxValue: 800,
} as GraphItem;

export const areaGraphWithSeries = {
  ...areaGraph,
  series: createChartSeriesData([{ data: solarWindSpeedDummyData }], areaGraph),
};

const logGraph = {
  id: '4-wind-density-logarithmicGraph',
  title: 'Solar Wind Density (1/cm\u00B3)',
  graphType: 'LOG',
  params: [
    {
      stream: 'rtsw_wind',
      parameter: 'proton_density',
    },
  ],
  columns: ['lineSolarWindDensity'],
  timeWidth: '1s',
  yMinValue: 0.1,
  yMaxValue: 1000,
  threshold: [{ title: '', value: 10 }],
} as GraphItem;

export const logGraphWithSeries = {
  ...logGraph,
  series: createChartSeriesData(
    [{ data: solarWindDensityDummyData }],
    logGraph,
  ),
};

const bandGraph = {
  id: '1-rtsw-bandGraph',
  title: 'Interplanetary Magnetic Field Bt and Bz (nT)',
  graphType: 'BAND',
  params: [
    { stream: 'rtsw_mag', parameter: 'bt' },
    { stream: 'rtsw_mag', parameter: 'bz_gsm' },
  ],
  columns: ['primaryValue', 'secondaryValue'],
  timeWidth: '1s',
  threshold: [{ title: '', value: -20 }],
  yMinValue: -25,
  yMaxValue: 25,
} as GraphItem;

export const bandGraphWithSeries = {
  ...bandGraph,
  series: createChartSeriesData(
    [
      { data: interplanetaryMagneticFieldBt },
      { data: interplanetaryMagneticFieldBz },
    ],
    bandGraph,
  ),
};
