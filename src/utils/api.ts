/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import axios from 'axios';

import {
  SWEvent,
  Bulletin,
  TimeseriesParams,
  TimeseriesResponseData,
  StreamResponse,
  StreamParams,
  SWErrors,
} from '../types';
import {
  kpIndexDummyData,
  kpIndexForecastDummyData,
  interplanetaryMagneticFieldBt,
  interplanetaryMagneticFieldBz,
  solarWindSpeedDummyData,
  solarWindPressureDummyData,
  xRayFluxDummyData,
  solarWindDensityDummyData,
} from './DummyData';
import { fetchEvent, fetchFakeNewNotifications } from './fakedata';

// TODO: (Sander de Snaijer 2020-07-16) remove after axios instance
const fakePromise = (withRandomError: boolean): Promise<void> =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (!withRandomError) return resolve();
      const randomError = Math.floor(Math.random() * 4) + 1;
      return randomError === 4
        ? reject(new Error('something went wrong'))
        : resolve();
    }, 300);
  });

// TODO: (Sander de Snaijer 2020-07-16) replace with real axios instance
const fakeAxiosInstance = {
  // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
  get: (_url, _params?): Promise<void> => fakePromise(false),
  // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
  post: (_url, _params?): Promise<void> => fakePromise(false),
};

// cancel tokens
const { CancelToken } = axios;
const storedCancels = {};
export const cancelRequestById = (requestId: string): void => {
  if (requestId && storedCancels[requestId]) {
    storedCancels[requestId](SWErrors.USER_CANCELLED);
  }
};

export const createCancelRequestId = (param: TimeseriesParams): string =>
  `-${param.stream}-${param.parameter}`;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const createApi = (url: string): any => {
  const axiosInstance = axios.create({
    baseURL: url,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    timeout: 10000,
  });

  const api = {
    // api data
    getTimeSeries: (
      params: TimeseriesParams,
      requestId: string,
    ): Promise<{ data: TimeseriesResponseData }> => {
      cancelRequestById(requestId);
      return axiosInstance.get('/timeseries/data', {
        params,
        cancelToken: new CancelToken(cancelToken => {
          storedCancels[requestId] = cancelToken;
        }),
      });
    },

    getTimeSeriesMultiple: (
      params: TimeseriesParams[],
    ): Promise<{ data: TimeseriesResponseData }[]> => {
      return Promise.all(
        params.map(param =>
          api.getTimeSeries(param, createCancelRequestId(param)),
        ),
      );
    },

    getStreams: (
      params: StreamParams,
      requestId?: string,
    ): Promise<{ data: StreamResponse[] }> => {
      cancelRequestById(requestId);
      return axiosInstance.get('/timeseries/streams', {
        params,
      });
    },

    getBulletin: (bulletinId: string): Promise<{ data: Bulletin }> => {
      const params = !bulletinId ? { bulletinId } : null;
      return axiosInstance.get('/bulletin/getBulletin', { params });
    },

    getBulletinHistory: (): Promise<{ data: Bulletin[] }> => {
      return axiosInstance.get('/bulletin/getBulletinHistory');
    },

    getEventList: (params): Promise<{ data: SWEvent[] }> => {
      return axiosInstance.get('/notification/eventList', { params });
    },
    getEvent: (eventid): Promise<{ data: SWEvent }> => {
      return axiosInstance.get(`/notification/event/${eventid}`);
    },

    getNewNotifications: (): Promise<{ data: SWEvent[] }> => {
      return axiosInstance.get('/notification/newNotifications');
    },

    // POST
    issueNotification: (formData): Promise<void> => {
      return axiosInstance.post('/notification/store', { ...formData });
    },

    discardDraftNotification: (params): Promise<void> => {
      return axiosInstance.post('/notification/discard', null, { params });
    },

    setAcknowledged: (eventid): Promise<void> => {
      return axiosInstance.post(`/notification/acknowledged/${eventid}`);
    },

    // dummy calls
    fakeIssueNotification: (formData): Promise<void> => {
      return fakeAxiosInstance.post('/store', { ...formData });
    },

    fakeDiscardDraftNotification: (params): Promise<void> => {
      return fakeAxiosInstance.post('/discard', { params });
    },

    fakeSetAcknowledged: (eventid): Promise<void> => {
      return fakeAxiosInstance.post(`/acknowledged/${eventid}`);
    },

    getFakeTimeSeries: (
      params: TimeseriesParams[],
    ): Promise<{ data: TimeseriesResponseData }[]> => {
      const bundeledSeries = params.map(param => {
        const getDummyTimeSerie = (
          stream: string,
          parameter: string,
        ): TimeseriesResponseData => {
          switch (stream) {
            case 'kp':
              return kpIndexDummyData;
            case 'kpforecast':
              return kpIndexForecastDummyData;
            case 'rtsw_mag':
              return parameter === 'bt'
                ? interplanetaryMagneticFieldBt
                : interplanetaryMagneticFieldBz;
            case 'rtsw_wind': {
              if (param.parameter === 'proton_speed') {
                return solarWindSpeedDummyData;
              }
              if (param.parameter === 'proton_pressure') {
                return solarWindPressureDummyData;
              }
              if (param.parameter === 'proton_density') {
                return solarWindDensityDummyData;
              }
              return solarWindDensityDummyData;
            }
            case 'xray':
              return xRayFluxDummyData;
            default:
              return kpIndexDummyData;
          }
        };

        return fakeAxiosInstance.get('/fakeGetNewNotification').then(() => ({
          data: getDummyTimeSerie(param.stream, param.parameter),
        }));
      });

      return Promise.all(bundeledSeries);
    },
    getFakeEvent: (eventid): Promise<{ data: SWEvent }> =>
      fakeAxiosInstance.get(`/event/${eventid}`).then(() => ({
        data: fetchEvent(eventid),
      })),
    getFakeNewNotifications: (): Promise<{ data: SWEvent[] }> =>
      fakeAxiosInstance.get('/notification/newNotifications').then(() => ({
        data: fetchFakeNewNotifications(),
      })),
  };
  return api;
};
