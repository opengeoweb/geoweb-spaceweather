/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import { renderHook, act } from '@testing-library/react-hooks';
import { useApi } from './hooks';

describe('src/utils/hooks', () => {
  it('should return correct data retrieved from a successful api call', async () => {
    const data = ['test', 'test', 'test'];
    const request = (): Promise<{}> =>
      new Promise(resolve => resolve({ data }));

    let testResult;

    await act(async () => {
      const { result } = await renderHook(() => useApi(request));

      expect(result.current.isLoading).toBeTruthy();
      expect(result.current.error).toBeNull();
      expect(result.current.result).toBeNull();

      testResult = result;
    });

    act(() => {
      expect(testResult.current.isLoading).toBeFalsy();
      expect(testResult.current.error).toBeNull();
      expect(testResult.current.result).toEqual(data);
    });
  });

  it('should return correct result from a unsuccessful api call', async () => {
    const testError = new Error('test error');
    const request = (): Promise<{}> =>
      new Promise((resolve, reject) => reject(testError));

    let testResult;

    await act(async () => {
      const { result } = await renderHook(() => useApi(request));

      expect(result.current.isLoading).toBeTruthy();
      expect(result.current.error).toBeNull();
      expect(result.current.result).toBeNull();

      testResult = result;
    });

    act(() => {
      expect(testResult.current.isLoading).toBeFalsy();
      expect(testResult.current.error).toEqual(testError);
      expect(testResult.current.result).toBeNull();
    });
  });
});
