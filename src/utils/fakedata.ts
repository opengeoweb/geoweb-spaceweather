/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import { SWEvent } from '../types';

export const fakeNotifications = [
  { category: 'XRAY_RADIO_BLACKOUT' },
  { category: 'ELECTRON_FLUX' },
  { category: 'GEOMAGNETIC' },
  { category: 'XRAY_RADIO_BLACKOUT' },
  { category: 'GEOMAGNETIC' },
  { category: 'GEOMAGNETIC' },
  { category: 'PROTON_FLUX' },
  { category: 'XRAY_RADIO_BLACKOUT' },
  { category: 'PROTON_FLUX' },
];

// Event list containing fake events that gets retrieved when doing a get /eventlist request
export const fakeEventList: SWEvent[] = [
  {
    eventid: 'METRB1123454',
    category: 'PROTON_FLUX',
    originator: 'METOffice',

    lifecycles: {
      externalprovider: {
        eventid: 'METRB4',
        label: 'ALERT',
        owner: 'METOffice',
        firstissuetime: '2020-07-12 03:22',
        lastissuetime: '2020-07-12 03:22',
        threshold: '1.0 x 10^3',
        eventlevel: 'S2',
        state: 'issued',
        eventstart: '2020-07-12 04:00',
        eventend: '2020-07-12 06:00',
        canbe: [],
        notifications: [
          {
            eventid: 'METRB1123454',
            category: 'PROTON_FLUX',
            label: 'ALERT',
            changestateto: 'issued',
            neweventlevel: 'S2',
            threshold: '1.0 x 10^3',
            neweventstart: '2020-07-12 04:00',
            neweventend: '2020-07-12 06:00',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB1123454',
            srcEventId: 'METRB1123454',
            issuetime: '2020-07-12 03:22',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB4',
        label: 'WARNING',
        owner: 'KNMI',
        state: 'issued',
        eventstart: '2020-07-12 04:00',
        eventend: '2020-07-12 06:00',
        threshold: '1.0 x 10^3',
        draft: true,
        canbe: ['updated', 'extended', 'summarised'],
        notifications: [
          {
            eventid: 'METRB1123454',
            category: 'PROTON_FLUX',
            neweventlevel: 'S2',
            label: 'WARNING',
            changestateto: 'issued',
            draft: false,
            neweventstart: '2020-07-12 04:20',
            neweventend: '2020-07-12 06:20',
            threshold: '1.0 x 10^3',
            message:
              'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
            notificationid: 'DRAFTNOTIFICATION122455',
            srcEventId: 'METRB1123454',
          },
          {
            eventid: 'METRB1123454',
            category: 'PROTON_FLUX',
            neweventlevel: 'S2',
            label: 'WARNING',
            changestateto: 'issued',
            draft: true,
            neweventstart: '2020-07-12 04:20',
            neweventend: '2020-07-12 06:20',
            threshold: '1.0 x 10^3',
            message:
              'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
            notificationid: 'DRAFTNOTIFICATION122455',
            srcEventId: 'METRB1123454',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB2',
    category: 'GEOMAGNETIC',
    originator: 'METOffice',
    notacknowledged: true,

    lifecycles: {
      externalprovider: {
        eventid: 'METRB2',
        label: 'ALERT',
        eventlevel: 'G1',
        owner: 'METOffice',
        eventstart: '2020-07-14 11:20',
        eventend: '2020-07-14 12:55',
        threshold: '1.0 x 10^3',
        firstissuetime: '2020-07-14 11:13',
        lastissuetime: '2020-07-14 11:13',
        state: 'issued',
        canbe: ['updated', 'extended', 'summarised'],
        notifications: [
          {
            eventid: 'METRB2',
            category: 'GEOMAGNETIC',
            neweventlevel: 'G1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-14 11:20',
            neweventend: '2020-07-14 12:55',
            threshold: '1.0 x 10^3',
            message:
              '<b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB2MET_8373489',
            srcEventId: 'METRB2MET_8373489',
            issuetime: '2020-07-14 11:13',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB122',
    category: 'PROTON_FLUX',
    originator: 'METOffice',

    lifecycles: {
      externalprovider: {
        eventid: 'METRB122',
        label: 'WARNING',
        owner: 'METOffice',
        eventlevel: 'S1',
        firstissuetime: '2020-07-14 06:30',
        lastissuetime: '2020-07-14 06:30',
        eventstart: '2020-07-14 06:00',
        eventend: '2020-07-14 08:12',
        threshold: '1.0 x 10^3',
        state: 'issued',
        canbe: ['updated', 'extended', 'cancelled'],
        notifications: [
          {
            eventid: 'METRB122',
            category: 'PROTON_FLUX',
            neweventlevel: 'S1',
            label: 'WARNING',
            changestateto: 'issued',
            neweventstart: '2020-07-14 06:00',
            neweventend: '2020-07-14 08:12',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB1MET1_973454564568',
            srcEventId: 'METRB1MET1_973454564568',
            issuetime: '2020-07-14 06:30',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB122',
        label: 'WARNING',
        owner: 'METOffice',
        eventlevel: 'S1',
        firstissuetime: '2020-07-14 06:35',
        lastissuetime: '2020-07-14 06:35',
        eventstart: '2020-07-14 06:05',
        eventend: '2020-07-14 08:20',
        threshold: '1.0 x 10^3',
        state: 'issued',
        canbe: ['updated', 'extended', 'cancelled'],
        notifications: [
          {
            eventid: 'METRB122',
            category: 'PROTON_FLUX',
            neweventlevel: 'S1',
            label: 'WARNING',
            changestateto: 'issued',
            neweventstart: '2020-07-14 06:05',
            neweventend: '2020-07-14 08:20',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'KNMIMET1_973454564568',
            srcEventId: 'METRB1MET1_973454564568',
            issuetime: '2020-07-14 06:35',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB1',
    category: 'XRAY_RADIO_BLACKOUT',
    originator: 'METOffice',

    lifecycles: {
      externalprovider: {
        eventid: 'METRB1',
        label: 'ALERT',
        eventlevel: 'R1',
        owner: 'METOffice',
        firstissuetime: '2020-07-13 12:00',
        lastissuetime: '2020-07-13 12:00',
        eventstart: '2020-07-13 13:00',
        eventend: '',
        threshold: '1.0 x 10^3',
        state: 'issued',
        canbe: ['updated', 'extended', 'summarised'],
        notifications: [
          {
            eventid: 'METRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-13 13:00',
            neweventend: '',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB1MET1_9749374728',
            srcEventId: 'METRB1MET1_9749374728',
            issuetime: '2020-07-13 12:00',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB1',
        label: 'ALERT',
        owner: 'KNMI',
        firstissuetime: '2020-07-13 12:10',
        lastissuetime: '2020-07-13 14:58',
        state: 'issued',
        eventlevel: 'R1',
        eventstart: '2020-07-13 13:00',
        eventend: '',
        threshold: '1.0 x 10^3',
        canbe: ['updated', 'extended', 'summarised'],
        notifications: [
          {
            eventid: 'METRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            label: 'ALERT',
            changestateto: 'issued',
            neweventlevel: 'R1',
            neweventstart: '2020-07-13 13:00',
            neweventend: '',
            threshold: '1.0 x 10^3',
            message: 'NOTIFICATIE TEXT IN HET NEDERLANDS JAWEEEL',
            notificationid: 'METRB1KNMI_8474620372',
            srcEventId: 'METRB1MET1_9749374728',
            issuetime: '2020-07-13 12:10',
          },
          {
            eventid: 'METRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            label: 'ALERT',
            changestateto: 'issued',
            neweventlevel: 'R1',
            neweventstart: '2020-07-13 13:00',
            neweventend: '',
            threshold: '1.0 x 10^3',
            message: 'en een update voor ons jaweeeel',
            notificationid: 'METRB1KNMI_89847263494',
            srcEventId: 'METRB1MET1_9749374728',
            issuetime: '2020-07-13 14:58',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB13',
    category: 'XRAY_RADIO_BLACKOUT',
    originator: 'METOffice',
    notacknowledged: true,

    lifecycles: {
      externalprovider: {
        label: 'WARNING',
        eventid: 'METRB13',
        owner: 'METOffice',
        firstissuetime: '2020-07-13 03:05',
        lastissuetime: '2020-07-13 05:30',
        eventlevel: 'R1',
        state: 'cancelled',
        eventstart: '2020-07-13 03:00',
        eventend: '2020-07-13 05:12',
        threshold: '1.0 x 10^3',
        canbe: [],

        notifications: [
          {
            eventid: 'METRB13',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'WARNING',
            changestateto: 'issued',
            neweventstart: '2020-07-13 03:00',
            neweventend: '2020-07-13 05:12',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB13MET1_356456',
            srcEventId: 'METRB13MET1_356456',
            issuetime: '2020-07-13 03:05',
          },
          {
            eventid: 'METRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R2',
            label: 'WARNING',
            changestateto: 'ended',
            neweventstart: '2020-07-13 03:00',
            neweventend: '2020-07-13 05:30',
            threshold: '1.0 x 10^3',
            message: 'its done',
            notificationid: 'METRB1KNMI_34547657',
            srcEventId: 'METRB1MET1_9749374728',
            issuetime: '2020-07-13 05:30',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB13',
        label: 'WARNING',
        owner: 'KNMI',
        firstissuetime: '2020-07-13 03:05',
        lastissuetime: '2020-07-13 03:10',
        threshold: '1.0 x 10^3',
        eventlevel: 'R1',
        state: 'issued',
        eventstart: '2020-07-13 03:00',
        eventend: '2020-07-13 05:12',
        canbe: ['updated', 'extended', 'cancelled'],
        notifications: [
          {
            eventid: 'METRB13',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'WARNING',
            changestateto: 'issued',
            neweventstart: '2020-07-13 03:00',
            neweventend: '2020-07-13 05:12',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB13KNMI_365687',
            srcEventId: 'METRB13MET1_356456',
            issuetime: '2020-07-13 03:10',
          },
        ],
      },
    },
  },
  {
    eventid: 'KNMIRB1',
    category: 'XRAY_RADIO_BLACKOUT',
    originator: 'KNMI',

    lifecycles: {
      internalprovider: {
        eventid: 'KNMIRB1',
        label: 'ALERT',
        owner: 'KNMI',
        eventlevel: 'R1',
        firstissuetime: '2020-07-12 15:10',
        lastissuetime: '2020-07-13 00:25',
        state: 'summary',
        eventstart: '2020-07-12 19:00',
        eventend: '2020-07-13 00:20',
        threshold: '1.0 x 10^3',

        canbe: [],
        notifications: [
          {
            eventid: 'KNMIRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-12 19:00',
            neweventend: '2020-07-13 00:20',
            threshold: '1.0 x 10^3',
            message: 'Er staat dus echt iets heeel ergs te gebeuren zeg he',
            notificationid: 'KNMIB1KNMI_903847589',
            srcEventId: 'KNMIB1KNMI_903847589',
            issuetime: '2020-07-12 15:10',
          },
          {
            eventid: 'KNMIRB1',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'ALERT',
            changestateto: 'ended',
            neweventstart: '2020-07-12 19:00',
            neweventend: '2020-07-13 00:20',
            threshold: '1.0 x 10^3',
            message: 'Het is allemaall voorbij jongens, geen paniek',
            notificationid: 'METRB1KNMI_3656734',
            srcEventId: 'KNMIB1KNMI_903847589',
            issuetime: '2020-07-13 00:25',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB3',
    category: 'ELECTRON_FLUX',
    originator: 'METOffice',
    lifecycles: {
      externalprovider: {
        eventid: 'METRB3',
        label: 'ALERT',
        owner: 'METOffice',
        firstissuetime: '2020-07-09 06:13',
        lastissuetime: '2020-07-09 07:59',
        state: 'ended',
        eventstart: '2020-07-09 06:00',
        eventend: '2020-07-09 08:12',
        canbe: [],
        threshold: '1.0 x 10^3',
        notifications: [
          {
            eventid: 'METRB3',
            category: 'ELECTRON_FLUX',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-09 06:00',
            neweventend: '2020-07-09 08:12',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
            notificationid: 'METRB3MET1_9874795',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-09 06:13',
          },
          {
            eventid: 'METRB3',
            category: 'ELECTRON_FLUX',

            label: 'ALERT',
            changestateto: 'ended',
            neweventstart: '2020-07-09 06:00',
            neweventend: '2020-07-09 07:55',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>It is all over</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB3MET12_3453545',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-09 07:59',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB3',
        owner: 'KNMI',
        label: 'ALERT',
        firstissuetime: '2020-07-09 06:16',
        lastissuetime: '2020-07-09 08:06',
        state: 'ended',
        eventstart: '2020-07-09 06:00',
        eventend: '2020-07-09 07:55',
        threshold: '1.0 x 10^3',
        canbe: [],
        notifications: [
          {
            eventid: 'METRB3',
            category: 'ELECTRON_FLUX',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-09 06:00',
            neweventend: '2020-07-09 07:55',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Hier staat wat nederlandse tekst over de event die komt</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB3KNMI_4656778',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-09 06:19',
          },
          {
            eventid: 'METRB3',
            category: 'ELECTRON_FLUX',
            label: 'ALERT',
            changestateto: 'ended',
            neweventstart: '2020-07-09 06:00',
            neweventend: '2020-07-09 07:55',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>En het is allemaal voorbij</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB3MET12_3453545',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-09 08:06',
          },
        ],
      },
    },
  },
  {
    eventid: 'KNMIRB24',
    category: 'XRAY_RADIO_BLACKOUT',
    originator: 'KNMI',

    lifecycles: {
      internalprovider: {
        eventid: 'KNMIRB24',
        label: 'ALERT',
        owner: 'KNMI',
        firstissuetime: '2020-07-09 00:34',
        lastissuetime: '2020-07-09 00:34',
        eventstart: '2020-07-09 00:15',
        eventend: '2020-07-09 02:20',
        eventlevel: 'R1',
        threshold: '1.0 x 10^3',
        state: 'issued',
        canbe: ['updated', 'extended', 'summarised'],
        notifications: [
          {
            eventid: 'KNMIRB24',
            category: 'XRAY_RADIO_BLACKOUT',
            neweventlevel: 'R1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-09 00:15',
            neweventend: '2020-07-09 02:20',
            threshold: '1.0 x 10^3',
            message: 'Er staat dus echt iets heeel ergs te gebeuren zeg he',

            notificationid: 'KNMIRB24_234289',
            srcEventId: 'KNMIRB24_9435349',
            issuetime: '2020-07-09 00:34',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB4',
    category: 'GEOMAGNETIC',
    originator: 'METOffice',

    lifecycles: {
      externalprovider: {
        eventid: 'METRB4',
        label: 'ALERT',
        owner: 'METOffice',
        firstissuetime: '2020-07-08 03:22',
        lastissuetime: '2020-07-08 03:22',
        eventlevel: 'G1',
        threshold: '1.0 x 10^3',
        state: 'issued',
        eventstart: '2020-07-08 03:12',
        eventend: '2020-07-08 05:55',
        canbe: [],
        notifications: [
          {
            eventid: 'METRB4',
            category: 'GEOMAGNETIC',
            neweventlevel: 'G1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-08 03:12',
            neweventend: '2020-07-08 05:55',
            threshold: '1.0 x 10^3',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB4MET1_36456',
            srcEventId: 'METRB4MET1_36456',
            issuetime: '2020-07-08 03:22',
          },
        ],
      },
    },
  },
  {
    eventid: 'METRB25',
    category: 'ELECTRON_FLUX',
    originator: 'METOffice',
    lifecycles: {
      externalprovider: {
        eventid: 'METRB25',
        label: 'ALERT',
        owner: 'METOffice',
        firstissuetime: '2020-07-07 06:13',
        lastissuetime: '2020-07-07 07:59',
        state: 'ended',
        eventlevel: 'G1',
        eventstart: '2020-07-07 06:00',
        threshold: '',
        eventend: '2020-07-07 08:12',
        canbe: [],
        notifications: [
          {
            eventid: 'METRB25',
            category: 'ELECTRON_FLUX',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-07 06:00',
            threshold: '',
            neweventend: '2020-07-07 08:12',
            message:
              '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB3MET1_9874795',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-07 06:13',
          },
          {
            eventid: 'METRB25',
            category: 'ELECTRON_FLUX',
            label: 'ALERT',
            changestateto: 'ended',
            neweventstart: '2020-07-09 06:00',
            neweventend: '2020-07-09 07:55',
            threshold: '',
            message:
              '<p><b>It is all over</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB25T12_3453545',
            srcEventId: 'METRB3MET1_9874795',
            issuetime: '2020-07-07 07:59',
          },
        ],
      },
      internalprovider: {
        eventid: 'METRB25',
        owner: 'KNMI',
        label: 'ALERT',
        firstissuetime: '2020-07-07 06:16',
        lastissuetime: '2020-07-07 08:06',
        state: 'issued',
        eventstart: '2020-07-07 06:00',
        eventend: '2020-07-07 07:55',
        threshold: '',
        draft: true,
        canbe: [],
        notifications: [
          {
            eventid: 'METRB25',
            category: 'ELECTRON_FLUX',
            neweventlevel: 'EF1',
            label: 'ALERT',
            changestateto: 'issued',
            neweventstart: '2020-07-07 06:00',
            neweventend: '2020-07-07 07:55',
            message:
              '<p><b>Hier staat wat nederlandse tekst over de event die komt</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',

            notificationid: 'METRB3KNMI_4656778',
            srcEventId: 'METRB3MET1_9874795',
            threshold: '',
            issuetime: '2020-07-07 06:19',
          },
          {
            eventid: 'METRB25',
            category: 'ELECTRON_FLUX',
            neweventlevel: 'EF1',
            label: 'ALERT',
            neweventstart: '2020-07-07 06:00',
            neweventend: '2020-07-07 07:55',
            draft: true,
            changestateto: 'ended',
            message: 'Allemaal voorbij!',

            notificationid: 'METRB3MET12_3453545',
            srcEventId: 'METRB3MET1_9874795',
            threshold: '',
            issuetime: '2020-07-07 08:06',
          },
        ],
      },
    },
  },
];

// get event list /eventlist - possible option to pass: category
export const fetchEventList = (params): SWEvent[] => {
  // api call
  // fake retrieval
  let eventList = [];
  if (params.category) {
    eventList = fakeEventList.filter(
      event => event.category === params.category,
    );
  } else if (params.originator && params.originator === 'KNMI') {
    eventList = fakeEventList.filter(event => event.originator === 'KNMI');
  } else {
    eventList = fakeEventList;
  }

  return eventList;
};

// get event /event - possible option to pass: category
export const fetchEvent = (eventid): SWEvent => {
  // api call
  // fake retrieval

  return fakeEventList.find(fakeEvent => fakeEvent.eventid === eventid);
};

// get whether there are new notifications (unacknowledged) /isNewNotification
export const fetchFakeNewNotifications = (): SWEvent[] => {
  return fakeEventList.filter(
    event => event.originator === 'METOffice' && event.notacknowledged,
  );
};
