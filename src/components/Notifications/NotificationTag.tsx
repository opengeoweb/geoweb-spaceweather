/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';

import { Box, Typography } from '@material-ui/core';

const getTagStyleColors = (
  type: string,
  colors: { warning: string; alert: string; summary: string; cancel: string },
): string => {
  if (type === 'Warning') {
    return colors.warning;
  }
  if (type === 'Alert') {
    return colors.alert;
  }
  if (type === 'Summary') {
    return colors.summary;
  }
  return colors.cancel;
};

export const getNotificationTagContent = (
  label: string,
  action = 'issued',
): string => {
  switch (label) {
    case 'WARNING':
      if (action === 'ended') {
        return 'Cancelled';
      }
      if (action === 'expired') {
        return 'Expired';
      }
      return 'Warning';

    case 'ALERT':
    default:
      if (action === 'ended') {
        return 'Summary';
      }
      return 'Alert';
  }
};

const tagStyle = (type: string): {} => {
  // Set color to grey for summary

  const bgcolor = getTagStyleColors(type, {
    cancel: '#f8f9f9',
    summary: '#f8f9f9',
    warning: '#fff8eb',
    alert: '#f9e8ea',
  });
  const borderColor = getTagStyleColors(type, {
    cancel: '#9b9fb0',
    summary: '#9d9df2',
    warning: '#ffa800',
    alert: '#f24a00',
  });

  return {
    bgcolor,
    borderColor,
    m: 0,
    p: 0.5,
    border: 2,
    borderRadius: 4,
    style: { width: '70px', height: '20px' },
    color: '#051039',
    maxHeight: '1em',
  };
};

interface NotificationTagProps {
  type: string;
}

export const NotificationTag: React.FC<NotificationTagProps> = ({
  type,
}: NotificationTagProps) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      {...tagStyle(type)}
      data-testid="notificationTag"
    >
      <Typography variant="caption">{type}</Typography>
    </Box>
  );
};

export default NotificationTag;
