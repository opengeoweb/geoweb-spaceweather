/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Grid,
  Typography,
  Card,
  CardContent,
  Button,
  makeStyles,
  ListItem,
} from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import NotificationsIcon from '@material-ui/icons/Notifications';
import moment from 'moment';

import NotificationTag from './NotificationTag';
import { EventCategory, SWEvent } from '../../types';

const useStyles = makeStyles({
  cardWrapper: {
    width: '100%',
  },
  cardContent: {
    padding: 14,
    '&:last-child': {
      paddingBottom: 14,
    },
  },
  notificationListItem: {
    padding: '2px',
  },
});

const getTag = (lifeCycle): string => {
  if (lifeCycle.label === 'WARNING') {
    if (lifeCycle.state === 'issued') {
      return 'Warning';
    }
    return lifeCycle.state === 'expired' ? 'Expired' : 'Cancelled';
  }

  return lifeCycle.state === 'issued' ? 'Alert' : 'Summary';
};

interface NotificationRowProps {
  event: SWEvent;
  onNotificationRowClick: (event: SWEvent) => void;
}

export const NotificationRow: React.FC<NotificationRowProps> = ({
  event,
  onNotificationRowClick,
}: NotificationRowProps) => {
  const classes = useStyles();

  const externalProviderLifeCycle =
    event.originator !== 'KNMI'
      ? event.lifecycles.externalprovider
      : {
          firstissuetime: '',
          lastissuetime: '',
          state: '',
          label: '',
          eventlevel: '',
        };

  const internalProviderLifeCycle =
    event.lifecycles.internalprovider !== undefined
      ? event.lifecycles.internalprovider
      : {
          lastissuetime: '',
          state: '',
          draft: false,
          label: '',
          eventlevel: '',
        };

  const internalProviderTag = getTag(internalProviderLifeCycle);
  const externalProviderTag = getTag(externalProviderLifeCycle);

  const eventlevel =
    event.originator !== 'KNMI'
      ? externalProviderLifeCycle.eventlevel
      : internalProviderLifeCycle.eventlevel;

  return (
    <ListItem
      button
      onClick={(): void => {
        onNotificationRowClick(event);
      }}
      key={event.eventid}
      className={classes.notificationListItem}
      data-testid="notificationRow-listitem"
    >
      <Card elevation={0} variant="outlined" className={classes.cardWrapper}>
        <CardContent className={classes.cardContent}>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs={2} sm={3}>
              <Typography variant="subtitle1">
                {`${EventCategory[event.category]}${
                  eventlevel ? ` - ${eventlevel}` : ''
                }`}
              </Typography>
            </Grid>
            <Grid item xs={10} sm={9}>
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={6}>
                  {event.originator !== 'KNMI' ? (
                    <Grid
                      container
                      alignItems="center"
                      spacing={1}
                      data-testid="notificationRow-externalCycle"
                    >
                      <Grid item xs={7}>
                        <Typography variant="body2">
                          {moment
                            .utc(externalProviderLifeCycle.lastissuetime)
                            .format('YYYY-MM-DD HH:mm')
                            .concat(' UTC')}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs
                        data-testid="notificationRow-externalNotificationTag"
                      >
                        <NotificationTag type={externalProviderTag} />
                      </Grid>
                    </Grid>
                  ) : null}
                </Grid>
                <Grid item xs={6}>
                  <Grid container alignItems="center" spacing={1}>
                    {event.notacknowledged === true && (
                      <Grid item xs>
                        <NotificationsIcon
                          fontSize="small"
                          color="action"
                          data-testid="newNotificationRowIcon"
                        />
                      </Grid>
                    )}
                    <Grid item xs={7}>
                      <Typography
                        variant="body2"
                        data-testid="notificationRow-internalLastIssueTime"
                      >
                        {event.notacknowledged !== true &&
                          internalProviderLifeCycle.lastissuetime &&
                          moment
                            .utc(internalProviderLifeCycle.lastissuetime)
                            .format('YYYY-MM-DD HH:mm')
                            .concat(' UTC')}
                      </Typography>
                    </Grid>
                    {internalProviderLifeCycle.draft !== undefined &&
                    internalProviderLifeCycle.draft === true ? (
                      <Grid item xs>
                        <Grid container alignItems="center" justify="flex-end">
                          <Button
                            color="secondary"
                            size="small"
                            data-testid="notificationRow-draft"
                            startIcon={
                              <CreateIcon fontSize="small" color="secondary" />
                            }
                          >
                            Edit
                          </Button>
                        </Grid>
                      </Grid>
                    ) : (
                      <Grid
                        item
                        xs
                        data-testid="notificationRow-internalNotificationTag"
                      >
                        {event.notacknowledged !== true &&
                        internalProviderLifeCycle.lastissuetime ? (
                          <NotificationTag type={internalProviderTag} />
                        ) : null}
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </ListItem>
  );
};

export default NotificationRow;
