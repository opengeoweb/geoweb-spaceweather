/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Grid, makeStyles, Typography, Button } from '@material-ui/core';

import { NotificationList } from './NotificationList';
import { LifeCycleDialog } from '../LifeCycleDialog';
import { SWEvent, SWNotification } from '../../types';
import { useApi } from '../../utils/hooks';
import { useNotificationTriggerContext } from '../NotificationTrigger';
import { useApiContext } from '../ApiContext/ApiContext';

const useStyles = makeStyles({
  NotificationHeader: {
    textAlign: 'center',
  },
  ButtonRow: { marginTop: '8px', marginRight: '32px' },
});

interface NotificationProps {
  requestPromiseStore?: (formData: SWNotification) => Promise<void>;
  requestPromiseAcknowledge?: (eventId: string) => Promise<void>;
  requestPromiseDiscardDraft?: (notificationId: string) => Promise<void>;
}

/**
 * Notifications
 * View incoming notifications and issue new notifications following the notification lifecycle
 *
 * @example
 * ``` <Notifications /> ```
 */
const Notifications: React.FC<NotificationProps> = ({
  requestPromiseStore,
  requestPromiseAcknowledge,
  requestPromiseDiscardDraft,
}: NotificationProps) => {
  const classes = useStyles();
  const { api } = useApiContext();

  const requestAcknowledge = requestPromiseAcknowledge || api.setAcknowledged;

  const {
    notificationTriggers: newNotifications,
    onFetchNewNotificationTriggerData,
  } = useNotificationTriggerContext();

  const [dialogEvent, setDialogEvent] = React.useState(null);
  const [lifeCycleDialogMode, setLifeCycleDialogMode] = React.useState('new');
  const [lifeCycleDialogOpen, setLifeCycleDialogOpen] = React.useState(false);

  const [tabValue, setTabValue] = React.useState('ALL');

  // Retrieve eventList
  const params =
    tabValue === 'KNMI' ? { originator: tabValue } : { category: tabValue };
  const {
    error: errorList,
    isLoading: isLoadingList,
    result: eventList,
    clearResults,
    fetchApiData: fetchNewEventList,
  } = useApi(api.getEventList, tabValue === 'ALL' ? {} : params);

  const getNewEventList = (): void => {
    const newparams =
      tabValue === 'KNMI' ? { originator: tabValue } : { category: tabValue };
    fetchNewEventList(tabValue === 'ALL' ? {} : newparams);
  };

  React.useEffect(() => {
    if (clearResults) clearResults();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabValue]);

  React.useEffect(() => {
    getNewEventList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newNotifications]);

  const toggleDialogOpen = (): void => {
    // Refresh list on closing dialog
    if (lifeCycleDialogOpen === true) getNewEventList();
    setLifeCycleDialogOpen(!lifeCycleDialogOpen);
  };

  React.useEffect(() => {
    if (lifeCycleDialogMode !== 'new') {
      toggleDialogOpen();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lifeCycleDialogMode]);

  const toggleCreateNotification = (): void => {
    setLifeCycleDialogMode('new');
    toggleDialogOpen();
  };

  const handleNotificatioNRowClick = (event: SWEvent): void => {
    if (lifeCycleDialogMode !== event.eventid) {
      setLifeCycleDialogMode(event.eventid);
      setDialogEvent(event);
      if (event.notacknowledged === true) {
        requestAcknowledge(event.eventid)
          .then(() => {
            onFetchNewNotificationTriggerData();
          })
          // eslint-disable-next-line no-console
          .catch(e => console.warn(e));
      }
    } else {
      toggleDialogOpen();
    }
  };

  return (
    <Grid container spacing={1} style={{ height: '100%' }}>
      <Grid item xs={12}>
        <Typography variant="subtitle1" className={classes.NotificationHeader}>
          Notifications
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <NotificationList
          tabValue={tabValue}
          handleNotificatioNRowClick={handleNotificatioNRowClick}
          onChangeTabValue={setTabValue}
          newNotifications={newNotifications}
          eventList={eventList || null}
          isLoading={isLoadingList}
          error={errorList}
        />
      </Grid>
      <Grid item xs={12}>
        <Grid container justify="flex-end">
          <Grid item className={classes.ButtonRow}>
            <Button
              size="small"
              variant="outlined"
              color="secondary"
              onClick={toggleCreateNotification}
              data-testid="notifications-newnotification"
            >
              Issue a KNMI notification
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {lifeCycleDialogOpen && (
        <LifeCycleDialog
          open={lifeCycleDialogOpen}
          toggleStatus={toggleDialogOpen}
          dialogMode={lifeCycleDialogMode}
          {...(lifeCycleDialogMode !== 'new' &&
            dialogEvent !== null && { event: dialogEvent })}
          requestPromiseStore={requestPromiseStore || api.issueNotification}
          requestPromiseDiscardDraft={
            requestPromiseDiscardDraft || api.discardDraftNotification
          }
        />
      )}
    </Grid>
  );
};

export default Notifications;
