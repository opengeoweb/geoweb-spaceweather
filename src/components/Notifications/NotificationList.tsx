/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';

import { Grid, List, makeStyles, Card, CardContent } from '@material-ui/core';
import { Skeleton, Alert, AlertTitle } from '@material-ui/lab';

import NotificationRow from './NotificationRow';
import NotificationTabs from './NotificationTabs';
import { SWEvent } from '../../types';

const maxHeight = '33vh';

const useStyles = makeStyles({
  notificationList: {
    padding: 0,
  },
  card: {
    boxShadow: 'inset 0 0 2px 0 rgba(0, 0, 0, 0.5)',
    backgroundColor: '#f1f1f1',
  },
  cardContent: {
    height: maxHeight,
    minHeight: '306px',
    padding: '4px 8px',
    overflow: 'scroll',
  },
});

interface NotificationListProps {
  tabValue: string;
  newNotifications: SWEvent[];
  eventList: SWEvent[];
  isLoading: boolean;
  error: Error;
  onChangeTabValue: (value: string) => void;
  handleNotificatioNRowClick: (event: SWEvent) => void;
}

export const NotificationList: React.FC<NotificationListProps> = ({
  tabValue,
  newNotifications,
  eventList,
  isLoading,
  error,
  onChangeTabValue,
  handleNotificatioNRowClick,
}: NotificationListProps) => {
  const classes = useStyles();

  const handleChange = (newValue): void => {
    onChangeTabValue(newValue);
  };

  return (
    <Grid container spacing={0}>
      <Grid item xs={12}>
        <NotificationTabs
          activeTab={tabValue}
          newNotifications={newNotifications}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={12}>
        <Card elevation={0} variant="outlined" className={classes.card}>
          <CardContent className={classes.cardContent}>
            {eventList && (
              <List className={classes.notificationList}>
                {eventList.map(event => {
                  return (
                    <NotificationRow
                      event={event}
                      onNotificationRowClick={(_event): void => {
                        handleNotificatioNRowClick(_event);
                      }}
                      key={event.eventid}
                    />
                  );
                })}
              </List>
            )}
            {isLoading && (
              <Skeleton
                data-testid="notificationList-skeleton"
                variant="rect"
                height="100%"
                width="100%"
              />
            )}
            {error && (
              <Alert severity="error" data-testid="notificationList-alert">
                <AlertTitle>{error.message}</AlertTitle>
              </Alert>
            )}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default NotificationList;
