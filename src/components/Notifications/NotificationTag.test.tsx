/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';

import { render } from '@testing-library/react';
import { NotificationTag, getNotificationTagContent } from './NotificationTag';

describe('src/components/NotificationTag/NotificationTag', () => {
  it('should show the correct title for the tag', () => {
    const props = {
      type: 'Alert',
    };
    const { getByTestId } = render(<NotificationTag {...props} />);

    expect(getByTestId('notificationTag')).toBeTruthy();
    expect(getByTestId('notificationTag').textContent).toEqual(props.type);
  });
});

describe('src/components/NotificationTag/NotificationTag/getNotificationTagContent', () => {
  it('should return the appropriate label for the Tag', () => {
    expect(getNotificationTagContent('ALERT', 'issued')).toEqual('Alert');
    expect(getNotificationTagContent('ALERT')).toEqual('Alert');
    expect(getNotificationTagContent('ALERT', 'ended')).toEqual('Summary');
    expect(getNotificationTagContent('WARNING', 'issued')).toEqual('Warning');
    expect(getNotificationTagContent('WARNING')).toEqual('Warning');
    expect(getNotificationTagContent('WARNING', 'ended')).toEqual('Cancelled');
    expect(getNotificationTagContent('WARNING', 'expired')).toEqual('Expired');
  });
});
