/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';

import { render, fireEvent } from '@testing-library/react';
import { NotificationTabs } from './NotificationTabs';
import { mockEvent } from '../../utils/__mocks__/hooks';

describe('src/components/NotificationTabs/NotificationTabs', () => {
  it('should call onChange when selecting another tab and have the passed tab selected', async () => {
    const props = {
      activeTab: 'KNMI',
      newNotifications: [],
      onChange: jest.fn(),
    };
    const { getByTestId } = render(<NotificationTabs {...props} />);

    expect(getByTestId('newNotificationTabKNMI').classList).toContain(
      'Mui-selected',
    );
    expect(getByTestId('newNotificationTabAll').classList).not.toContain(
      'Mui-selected',
    );

    const KNMITab = getByTestId('newNotificationTabKNMI');

    fireEvent.click(KNMITab);

    expect(props.onChange).toHaveBeenCalledWith('KNMI');
  });
  it('should show no bell if no new notifications', () => {
    const props = {
      activeTab: 'ALL',
      newNotifications: [],
      onChange: jest.fn(),
    };
    const { queryByTestId, getByTestId } = render(
      <NotificationTabs {...props} />,
    );

    expect(queryByTestId('newNotificationTabIcon')).toBeFalsy();
    expect(getByTestId('newNotificationTabAll').classList).toContain(
      'Mui-selected',
    );
    expect(getByTestId('newNotificationTabKNMI').classList).not.toContain(
      'Mui-selected',
    );
  });
  it('should show bell if new notifications', () => {
    const props = {
      activeTab: 'ALL',
      newNotifications: [mockEvent],
      onChange: jest.fn(),
    };
    const { queryByTestId } = render(<NotificationTabs {...props} />);

    expect(queryByTestId('newNotificationTabIcon')).toBeTruthy();
  });
});
