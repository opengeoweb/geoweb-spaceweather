/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import moment from 'moment';

import { ContentDialog } from '../ContentDialog';
import LifeCycleEdit from './LifeCycleEdit';
import LifeCycleDisplay from './LifeCycleDisplay';
import { EventCategory, SWEvent, SWNotification } from '../../types';
import { useApiContext } from '../ApiContext/ApiContext';

interface LifeCycleDialogProps {
  open: boolean;
  toggleStatus: Function;
  dialogMode?: string;
  event?: SWEvent;
  requestPromiseStore?: (formData: SWNotification) => Promise<void>;
  requestPromiseEvent?: (eventId: string) => Promise<{ data: SWEvent }>;
  requestPromiseDiscardDraft?: (notificationId: string) => Promise<void>;
}

export const LifeCycleDialog: React.FC<LifeCycleDialogProps> = ({
  open,
  toggleStatus,
  dialogMode = 'new',
  event = null,
  requestPromiseEvent,
  requestPromiseStore,
  requestPromiseDiscardDraft,
}: LifeCycleDialogProps) => {
  const [dialogSize, setDialogSize] = React.useState('lg');
  const [isFormChanged, setFormIsChanged] = React.useState(false);

  const onChangeForm = (hasChanged: boolean): void => {
    setFormIsChanged(hasChanged);
  };

  React.useEffect(() => {
    if (dialogMode === 'new' || (event && event.originator === 'KNMI')) {
      setDialogSize('sm');
    } else {
      setDialogSize('lg');
    }
  }, [dialogMode, event]);

  const { api } = useApiContext();

  const titleTime =
    event &&
    event.lifecycles.internalprovider &&
    event.lifecycles.internalprovider.draft !== true
      ? moment
          .utc(
            event.lifecycles.externalprovider
              ? event.lifecycles.externalprovider.firstissuetime
              : event.lifecycles.internalprovider.firstissuetime,
          )
          .format('YYYY-MM-DD HH:mm')
          .concat(' UTC')
      : 'Draft';

  const onToggleDialog = (formSaved = false): void => {
    if (isFormChanged && !formSaved) {
      // eslint-disable-next-line no-alert, no-restricted-globals
      if (confirm('Changes that you made may not be saved.')) {
        toggleStatus();
      }
    } else {
      toggleStatus();
    }
  };

  return (
    <ContentDialog
      open={open}
      toggleStatus={onToggleDialog}
      data-testid="lifecycle-dialog"
      title={
        dialogMode !== 'new'
          ? `${EventCategory[event.category]}: ${titleTime}`
          : 'New Notification'
      }
      // @ts-ignore
      maxWidth={dialogSize}
      fullWidth
      disableEscapeKeyDown
      onClose={onToggleDialog}
    >
      {dialogMode === 'new' && (
        <LifeCycleEdit
          toggleDialogOpen={onToggleDialog}
          notificationTag="Alert"
          baseNotificationType="new"
          allowEndDateInPast={false}
          requestPromiseStore={requestPromiseStore || api.issueNotification}
          requestPromiseDiscardDraft={
            requestPromiseDiscardDraft || api.discardDraftNotification
          }
          onFormChange={onChangeForm}
        />
      )}
      {dialogMode !== 'new' && (
        <LifeCycleDisplay
          eventId={dialogMode}
          toggleDialogOpen={onToggleDialog}
          requestPromiseEvent={requestPromiseEvent || api.getEvent}
          requestPromiseStore={requestPromiseStore || api.issueNotification}
          requestPromiseDiscardDraft={
            requestPromiseDiscardDraft || api.discardDraftNotification
          }
          onFormChange={onChangeForm}
        />
      )}
    </ContentDialog>
  );
};

export default LifeCycleDialog;
