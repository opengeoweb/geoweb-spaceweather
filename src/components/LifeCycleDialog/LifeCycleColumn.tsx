/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Grid,
  Typography,
  List,
  ListItem,
  ListSubheader,
  makeStyles,
} from '@material-ui/core';
import { SubdirectoryArrowRight } from '@material-ui/icons';
import moment from 'moment';

import { SWEventLifeCycle } from '../../types';
import { NotificationTag } from '../Notifications';
import { getNotificationTagContent } from '../Notifications/NotificationTag';

const useStyles = makeStyles({
  NotificationHeader: { marginTop: '15px', marginBottom: '10px' },
});

interface LifeCycleColumnProps {
  lifeCycle: SWEventLifeCycle;
}

export const LifeCycleColumn: React.FC<LifeCycleColumnProps> = ({
  lifeCycle,
}: LifeCycleColumnProps) => {
  const classes = useStyles();

  return (
    <List data-testid="lifecycle-column">
      <ListSubheader disableSticky>
        <Typography variant="body2">{lifeCycle.owner}</Typography>
      </ListSubheader>

      {lifeCycle.notifications &&
        lifeCycle.notifications
          .filter(
            notification =>
              // Remove all drafts, they should not be displayed
              !notification.draft,
          )
          .map((notification, key) => {
            const eventStartFormat = moment
              .utc(notification.neweventstart)
              .format('YYYY-MM-DD HH:mm')
              .concat(' UTC');
            const eventEndFormat =
              notification.neweventend && notification.neweventend !== ''
                ? moment
                    .utc(notification.neweventend)
                    .format('YYYY-MM-DD HH:mm')
                    .concat(' UTC')
                : null;

            return (
              <ListItem
                // eslint-disable-next-line react/no-array-index-key
                key={`${notification.notificationid}-${notification.issuetime}-${key}`}
                data-testid={`${notification.notificationid}-${notification.issuetime}`}
              >
                <Grid container spacing={1}>
                  <Grid item xs={12} className={classes.NotificationHeader}>
                    {key !== 0 && (
                      <SubdirectoryArrowRight style={{ color: '#0075a9' }} />
                    )}
                    <Grid container data-testid="lifecycle-column-notification">
                      <Grid item xs={10}>
                        <Typography variant="overline">Issue time</Typography>
                        <Typography variant="body1">
                          {moment
                            .utc(notification.issuetime)
                            .format('YYYY-MM-DD HH:mm')
                            .concat(' UTC')}
                        </Typography>
                      </Grid>
                      <Grid item xs={2}>
                        <NotificationTag
                          type={getNotificationTagContent(
                            notification.label,
                            notification.changestateto,
                          )}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid item xs={5}>
                        <Typography variant="overline">Event Level</Typography>
                        <Typography variant="body1">
                          {notification.neweventlevel}
                        </Typography>
                      </Grid>
                      <Grid item xs={5}>
                        <Typography variant="overline">Threshold</Typography>
                        <Typography variant="body1">
                          {notification.threshold}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid item xs={5}>
                        <Typography variant="overline">Start</Typography>
                        <Typography>{eventStartFormat}</Typography>
                      </Grid>
                      {eventEndFormat && (
                        <Grid item xs={5}>
                          <Typography variant="overline">End</Typography>
                          <Typography data-testid="lifecycle-column-eventend">
                            {eventEndFormat}
                          </Typography>
                        </Grid>
                      )}
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="body2"
                      data-testid="lifecycle-column-message"
                      dangerouslySetInnerHTML={{
                        __html: notification.message,
                      }}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            );
          })}
    </List>
  );
};

export default LifeCycleColumn;
