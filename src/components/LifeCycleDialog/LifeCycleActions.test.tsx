/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import LifeCycleActions from './LifeCycleActions';

jest.mock('../../utils/hooks');

describe('src/components/LifeCycleDialog/LifeCycleActions', () => {
  it('should display the correct buttons based on the passed actions and call the appropriate functions', () => {
    const props = {
      actions: ['updated', 'extended', 'summarised'],
      onAction: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <LifeCycleActions {...props} />,
    );

    expect(getByTestId('updateextend')).toBeTruthy();
    expect(getByTestId('summarise')).toBeTruthy();
    expect(queryByTestId('cancel')).toBeFalsy();

    fireEvent.click(getByTestId('summarise'));
    expect(props.onAction).toHaveBeenCalledWith('Summarise');
    fireEvent.click(getByTestId('updateextend'));
    expect(props.onAction).toHaveBeenCalledWith('Updateextend');
  });

  it('should display the correct buttons based on the passed actions and call the appropriate functions part II ', () => {
    const props = {
      actions: ['updated', 'cancelled'],
      onAction: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <LifeCycleActions {...props} />,
    );

    expect(getByTestId('updateextend')).toBeTruthy();
    expect(getByTestId('cancel')).toBeTruthy();
    expect(queryByTestId('summarise')).toBeFalsy();

    fireEvent.click(getByTestId('cancel'));
    expect(props.onAction).toHaveBeenCalledWith('Cancel');
    fireEvent.click(getByTestId('updateextend'));
    expect(props.onAction).toHaveBeenCalledWith('Updateextend');
  });
});
