/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import moment from 'moment';

import { render, waitFor } from '@testing-library/react';
import LifeCycleDialog from './LifeCycleDialog';
import {
  mockEvent,
  mockEventAcknowledgedExternalDraft,
} from '../../utils/__mocks__/hooks';
import { EventCategory } from '../../types';
import { ApiProvider } from '../ApiContext/ApiContext';

jest.mock('../../utils/hooks');

describe('src/components/LifeCycleDialog/LifeCycleDialog', () => {
  it('should show the correct title and content for a new notification', () => {
    const props = {
      open: true,
      dialogMode: 'new',
      event: null,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual('New Notification');
    expect(getByTestId('edit-lifecycle')).toBeTruthy();
  });

  it('should show the correct title and content for an existing notification', async () => {
    const props = {
      open: true,
      dialogMode: mockEvent.eventid,
      event: mockEvent,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${EventCategory[props.event.category]}: ${moment
      .utc(props.event.lifecycles.internalprovider.firstissuetime)
      .format('YYYY-MM-DD HH:mm')
      .concat(' UTC')}`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });
  it('should show the correct title and content for an existing notification with a draft', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternalDraft.eventid,
      event: mockEventAcknowledgedExternalDraft,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${EventCategory[props.event.category]}: Draft`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });
});
