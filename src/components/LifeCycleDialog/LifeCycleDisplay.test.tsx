/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react';
import LifeCycleDisplay from './LifeCycleDisplay';
import { createApi } from '../../utils/api';

const api = createApi('local.test');

jest.mock('../../utils/hooks');

describe('src/components/LifeCycleDialog/LifeCycleDisplay', () => {
  it('should display the lifecycle with an external and internal column for an external event', async () => {
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
      requestPromiseEvent: api.getEvent,
      requestPromiseStore: api.issueNotification,
      requestPromiseDiscardDraft: api.discardDraftNotification,
    };
    const { getByTestId, getAllByTestId } = render(
      <LifeCycleDisplay {...props} />,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getByTestId('display-lifecycle-internalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
    });
  });
  it('should display the action buttons (update/extend and cancelled) for an external WARNING for which internal notifications have already been issued', async () => {
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
      requestPromiseEvent: api.getEvent,
      requestPromiseStore: api.issueNotification,
      requestPromiseDiscardDraft: api.discardDraftNotification,
    };
    const { getByTestId, queryByTestId, getAllByTestId } = render(
      <LifeCycleDisplay {...props} />,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
      expect(getByTestId('cancel')).toBeTruthy();
      expect(getByTestId('updateextend')).toBeTruthy();
      expect(queryByTestId('summarise')).toBeFalsy();
      expect(queryByTestId('edit-lifecycle')).toBeFalsy();
    });
    // Then, when pressing the cancel button the edit fields should be displayed with the right tag
    fireEvent.click(getByTestId('cancel'));
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(getByTestId('lifecycle-display-internaleditaction')).toBeTruthy();
      expect(queryByTestId('lifecycle-display-internaldraftedit')).toBeFalsy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(getByTestId('notification-tag').textContent).toEqual('Cancelled');
    });
  });
  it('should display the edit section for an external alert for which no internal notifications have been issued', async () => {
    const props = {
      eventId: 'METRB3',
      toggleDialogOpen: jest.fn,
      requestPromiseEvent: api.getEvent,
      requestPromiseStore: api.issueNotification,
      requestPromiseDiscardDraft: api.discardDraftNotification,
    };
    const { getByTestId, queryByTestId, getAllByTestId } = render(
      <LifeCycleDisplay {...props} />,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(1);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(queryByTestId('lifecycle-display-internaleditaction')).toBeFalsy();
      expect(queryByTestId('lifecycle-display-internaldraftedit')).toBeFalsy();
    });
  });
  it('should display no external column if it is a internally issued event', async () => {
    const props = {
      eventId: 'METRB2',
      toggleDialogOpen: jest.fn,
      requestPromiseEvent: api.getEvent,
      requestPromiseStore: api.issueNotification,
      requestPromiseDiscardDraft: api.discardDraftNotification,
    };
    const { getByTestId, queryByTestId, getAllByTestId } = render(
      <LifeCycleDisplay {...props} />,
    );
    await waitFor(() => {
      expect(queryByTestId('display-lifecycle-externalcolumn')).toBeFalsy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(1);
      expect(getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
      expect(getByTestId('summarise')).toBeTruthy();
      expect(getByTestId('updateextend')).toBeTruthy();
      expect(queryByTestId('cancel')).toBeFalsy();
      expect(queryByTestId('edit-lifecycle')).toBeFalsy();
    });
  });
  it('should display the edit section for an external alert for which a draft has been saved', async () => {
    const props = {
      eventId: 'METRB1123453453454',
      toggleDialogOpen: jest.fn,
      requestPromiseEvent: api.getEvent,
      requestPromiseStore: api.issueNotification,
      requestPromiseDiscardDraft: api.discardDraftNotification,
    };
    const { getByTestId, queryByTestId, getAllByTestId } = render(
      <LifeCycleDisplay {...props} />,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(getByTestId('lifecycle-display-internaldraftedit')).toBeTruthy();
      expect(queryByTestId('lifecycle-display-internaleditaction')).toBeFalsy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
    });
  });
});
