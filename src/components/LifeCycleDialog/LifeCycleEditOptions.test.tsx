/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import moment from 'moment';
import LifeCycleEditOptions from './LifeCycleEditOptions';

import { mockEvent } from '../../utils/__mocks__/hooks';
import {
  constructBaseNotification,
  constructBaseNotificationWithValidation,
  MESSAGE_END_DATE_IN_FUTURE,
  MESSAGE_INVALID_FORMAT,
  MESSAGE_END_DATE_AFTER_START_DATE,
  MESSAGE_REQUIRED,
} from './utils';
import { EventCategory } from '../../types';

const dateFormat = 'YYYY-MM-DD HH:mm';

describe('src/components/LifeCycleDialog/LifeCycleEditOptions', () => {
  it('should display the correct date and time provided', () => {
    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const startDateInput = getByTestId('start-date-input');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      moment
        .utc(mockEvent.lifecycles.internalprovider.eventstart)
        .format(dateFormat),
    );

    const endDateInput = getByTestId('end-date-input');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      moment
        .utc(mockEvent.lifecycles.internalprovider.eventend)
        .format(dateFormat),
    );
  });

  it('should give an error message when end date is before start date', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T09:59:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };

    props.formData.neweventend = {
      value: end,
      isValid: false,
      errors: [MESSAGE_END_DATE_AFTER_START_DATE],
      validations: ['afterStartDate'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should give an error message when end date is equal to start date', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = start;
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };

    props.formData.neweventend = {
      value: end,
      isValid: false,
      errors: [MESSAGE_END_DATE_AFTER_START_DATE],
      validations: ['afterStartDate'],
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should not give an error message when end date is valid', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = moment
      .utc()
      .add(1, 'hour')
      .format('YYYY-MM-DD HH:mm');
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).not.toContain('Mui-error');
    expect(endDatePicker.querySelector('p')).toBeFalsy();
  });

  it('should give an error message when end date is in the past and its not allowed', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    props.formData.neweventend = {
      value: end,
      isValid: false,
      errors: [MESSAGE_END_DATE_IN_FUTURE],
      validations: ['dateInFuture'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should give an error message when end date is current datetime and end date in the past is not allowed', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = moment.utc().format();
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    props.formData.neweventend = {
      value: end,
      isValid: false,
      errors: [MESSAGE_END_DATE_IN_FUTURE],
      validations: ['dateInFuture'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should not give an error message when end date is empty', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;
    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    props.formData.neweventend.value = end;

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).not.toContain('Mui-error');
    expect(endDatePicker.querySelector('p')).toBeFalsy();
  });

  it('should not give an error message when end date is in the past and it is allowed', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).not.toContain('Mui-error');
    expect(endDatePicker.querySelector('p')).toBeFalsy();
  });

  it('should give an error message when start date is empty', () => {
    const end = moment
      .utc()
      .add(1, 'hour')
      .format('YYYY-MM-DD HH:mm');
    mockEvent.lifecycles.internalprovider.eventend = end;

    const formData = constructBaseNotificationWithValidation(
      constructBaseNotification(mockEvent, 'internalprovider'),
    );

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData,
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    props.formData.neweventstart = {
      value: null,
      isValid: false,
      errors: [MESSAGE_REQUIRED],
      validations: ['required'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const startDatePicker = getByTestId('start-date-picker');
    expect(
      startDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should give an error message when start date has wrong format', () => {
    const end = '2020-07-01T10:00:00Z';
    const start = '2020-07-0233 11:00';
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    props.formData.neweventstart = {
      value: start,
      isValid: false,
      errors: [MESSAGE_INVALID_FORMAT],
      validations: ['validDate'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const startDatePicker = getByTestId('start-date-picker');
    expect(
      startDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });

  it('should update the tag when changing the label', async () => {
    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(null, 'new'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: false,
    };
    const { getByTestId, findByText } = render(
      <LifeCycleEditOptions {...props} />,
    );
    expect(getByTestId('notification-tag').textContent).toEqual('Alert');
    fireEvent.mouseDown(getByTestId('label-select'));
    const menuItem = await findByText('Warning');
    fireEvent.click(menuItem);
    expect(getByTestId('notification-tag').textContent).toEqual('Warning');
    expect(props.onUpdateFormField).toHaveBeenCalled();
  });

  it('should be possible to select a different category', async () => {
    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(null, 'new'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId, findByText } = render(
      <LifeCycleEditOptions {...props} />,
    );
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    fireEvent.mouseDown(getByTestId('category-select'));
    const menuItem = await findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(menuItem);
    expect(props.onUpdateFormField).toHaveBeenCalled();
  });

  it('Different category and label input form should be hidden when eventType is disabled', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: true,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { queryByTestId } = render(<LifeCycleEditOptions {...props} />);
    expect(queryByTestId('category-select')).toBeFalsy();
    expect(queryByTestId('label-select')).toBeFalsy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('threshold-input')).toBeTruthy();
    expect(queryByTestId('start-date-input')).toBeTruthy();
    expect(queryByTestId('end-date-input')).toBeTruthy();
  });

  it('should be possible to select an event level', async () => {
    const baseNotifcation = constructBaseNotificationWithValidation(
      constructBaseNotification(mockEvent, 'internalprovider'),
    );

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: baseNotifcation,
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId, findByText } = render(
      <LifeCycleEditOptions {...props} />,
    );

    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItem = await findByText('G2');
    fireEvent.click(menuItem);
    expect(props.onUpdateFormField).toHaveBeenCalledWith({
      neweventlevel: {
        errors: [],
        isValid: true,
        validations: ['required'],
        value: 'G2',
      },
    });
  });

  it('should be possible to enter a threshold', async () => {
    const baseNotifcation = constructBaseNotificationWithValidation(
      constructBaseNotification(mockEvent, 'internalprovider'),
    );
    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: baseNotifcation,
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const thresholdInput = getByTestId('threshold-input');
    const newValue = 'test 123';
    fireEvent.change(thresholdInput, {
      target: { value: newValue },
    });

    expect(props.onUpdateFormField).toHaveBeenCalledWith({
      threshold: {
        ...baseNotifcation.threshold,
        value: newValue,
      },
    });
  });

  it('should be possible to change the startdate', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const baseNotifcation = constructBaseNotificationWithValidation(
      constructBaseNotification(mockEvent, 'internalprovider'),
    );

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: baseNotifcation,
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const startDateInput = getByTestId('start-date-input');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      moment.utc(start).format(dateFormat),
    );
    const newDate = '2020-06-28 09:00';
    fireEvent.change(startDateInput, {
      target: { value: newDate },
    });
    expect((startDateInput as HTMLInputElement).value).toEqual(newDate);
    expect(props.onUpdateFormField).toHaveBeenCalledWith({
      neweventstart: {
        ...baseNotifcation.neweventstart,
        value: newDate,
      },
    });
  });

  it('should be possible to change the enddate', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const baseNotifcation = constructBaseNotificationWithValidation(
      constructBaseNotification(mockEvent, 'internalprovider'),
    );

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: baseNotifcation,
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDateInput = getByTestId('end-date-input');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      moment.utc(end).format(dateFormat),
    );
    const newDate = '2020-07-28 09:00';
    fireEvent.change(endDateInput, {
      target: { value: newDate },
    });

    expect((endDateInput as HTMLInputElement).value).toEqual(newDate);
    expect(props.onUpdateFormField).toHaveBeenCalledWith({
      neweventend: {
        ...baseNotifcation.neweventend,
        value: newDate,
        isValid: false,
        errors: ['dateInFuture'],
      },
    });
  });

  it('Event level and treshold input fields should be hidden when summarizing', async () => {
    const props = {
      notificationTag: 'Summary',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { queryByTestId } = render(<LifeCycleEditOptions {...props} />);

    expect(queryByTestId('category-select')).toBeTruthy();
    expect(queryByTestId('label-select')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-input')).toBeFalsy();
    expect(queryByTestId('start-date-input')).toBeTruthy();
    expect(queryByTestId('end-date-input')).toBeTruthy();
  });

  it('Event level and treshold input fields should be hidden when cancelling', async () => {
    const props = {
      notificationTag: 'Cancelled',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };
    const { queryByTestId } = render(<LifeCycleEditOptions {...props} />);

    expect(queryByTestId('category-select')).toBeTruthy();
    expect(queryByTestId('label-select')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-input')).toBeFalsy();
    expect(queryByTestId('start-date-input')).toBeTruthy();
    expect(queryByTestId('end-date-input')).toBeTruthy();
  });

  it('should give an error message when end date has an invalid format', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-0233 11:00';
    mockEvent.lifecycles.internalprovider.eventstart = start;

    const props = {
      notificationTag: 'Alert',
      eventTypeDisabled: false,
      formData: constructBaseNotificationWithValidation(
        constructBaseNotification(mockEvent, 'internalprovider'),
      ),
      onUpdateFormField: jest.fn(),
      allowEndDateInPast: true,
    };

    props.formData.neweventend = {
      value: end,
      isValid: false,
      errors: [MESSAGE_INVALID_FORMAT],
      validations: ['validDate'],
    };

    const { getByTestId } = render(<LifeCycleEditOptions {...props} />);

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
  });
});
