/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import LifeCycleColumn from './LifeCycleColumn';
import {
  mockEventAcknowledgedExternalDraft,
  mockEventUnacknowledgedExternal,
} from '../../utils/__mocks__/hooks';

jest.mock('../../utils/hooks');

describe('src/components/LifeCycleDialog/LifeCycleColumn', () => {
  it('should display all notifications in the lifecyle except for the drafts', () => {
    const props = {
      lifeCycle: mockEventAcknowledgedExternalDraft.lifecycles.internalprovider,
    };

    const { getByTestId, getAllByTestId, queryByTestId } = render(
      <LifeCycleColumn {...props} />,
    );

    const {
      notifications,
    } = mockEventAcknowledgedExternalDraft.lifecycles.internalprovider;

    expect(getAllByTestId('lifecycle-column-notification').length).toEqual(2);
    expect(
      getByTestId(
        `${notifications[0].notificationid}-${notifications[0].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      getByTestId(
        `${notifications[1].notificationid}-${notifications[1].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      queryByTestId(
        `${notifications[2].notificationid}-${notifications[2].issuetime}`,
      ),
    ).toBeFalsy();
  });
  it('should display the notification message correctly', () => {
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles.externalprovider,
    };

    const {
      notifications,
    } = mockEventUnacknowledgedExternal.lifecycles.externalprovider;

    const { getByTestId } = render(<LifeCycleColumn {...props} />);

    expect(getByTestId('lifecycle-column-message').textContent).toEqual(
      notifications[0].message,
    );
  });
  it('should display the event end date if passed', () => {
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles.externalprovider,
    };

    const { getByTestId } = render(<LifeCycleColumn {...props} />);

    // If passed - eventend should be shown
    expect(getByTestId('lifecycle-column-eventend')).toBeTruthy();
  });
  it('should display the event end date if passed', () => {
    // if not passed - eventend should be hidden
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles.externalprovider,
    };
    props.lifeCycle.notifications[0].neweventend = '';

    const { queryByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryByTestId('lifecycle-column-eventend')).toBeFalsy();
  });
});
