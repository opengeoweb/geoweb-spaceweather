/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  makeStyles,
  Grid,
  TextField,
} from '@material-ui/core';

import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { Moment } from 'moment';
import { EventCategory, NotificationLabel } from '../../types';
import { NotificationTag } from '../Notifications';
import {
  SWNotificationWithValidation,
  getValidationMessage,
  validateField,
  ValidationField,
} from './utils';

const dateFormat = 'YYYY-MM-DD HH:mm';

const eventLevels = {
  XRAY_RADIO_BLACKOUT: ['R1', 'R2', 'R3', 'R4', 'R5'],
  GEOMAGNETIC: ['G1', 'G2', 'G3', 'G4', 'G5'],
  PROTON_FLUX: ['S1', 'S2', 'S3', 'S4', 'S5'],
  ELECTRON_FLUX: ['E'],
};

const useStyles = makeStyles({
  inputField: {
    width: 200,
    marginTop: '0px',
    marginBottom: '10px',
  },
});

interface LifeCycleEditOptionsProps {
  notificationTag: string;
  eventTypeDisabled?: boolean;
  formData: SWNotificationWithValidation;
  onUpdateFormField: (field: ValidationField) => void;
  allowEndDateInPast: boolean;
}

export const LifeCycleEditOptions: React.FC<LifeCycleEditOptionsProps> = ({
  notificationTag,
  eventTypeDisabled = false,
  formData,
  allowEndDateInPast,
  onUpdateFormField,
}: LifeCycleEditOptionsProps) => {
  const classes = useStyles();

  const [notificationTagText, setNotificationTagText] = React.useState(
    notificationTag,
  );

  const eventLevelThresholdDisabled =
    notificationTag === 'Summary' || notificationTag === 'Cancelled';

  const onChangeInput = (field, value): void => {
    const errors = validateField(value, formData[field].validations, formData);
    const updatedField = {
      [field]: {
        ...formData[field],
        value,
        errors,
        isValid: !errors.length,
      },
    } as ValidationField;

    onUpdateFormField(updatedField);
  };

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Grid container alignItems="center">
        {eventTypeDisabled ? null : (
          <Grid container>
            <Grid item xs={5}>
              <FormControl
                className={classes.inputField}
                error={!formData.category.isValid}
              >
                <InputLabel htmlFor="event-input-category">Category</InputLabel>
                <Select
                  value={formData.category.value}
                  onChange={(event): void =>
                    onChangeInput('category', event.target.value)
                  }
                  inputProps={{
                    name: 'category',
                    id: 'event-input-category',
                    SelectDisplayProps: {
                      'data-testid': 'category-select',
                    },
                  }}
                  variant="filled"
                >
                  {Object.keys(EventCategory).map(key => (
                    <MenuItem value={key} key={key}>
                      {EventCategory[key]}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={5}>
              <FormControl
                className={classes.inputField}
                error={!formData.label.isValid}
              >
                <InputLabel htmlFor="type-input-label">Label</InputLabel>
                <Select
                  value={formData.label.value}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    onChangeInput('label', e.target.value);
                    setNotificationTagText(
                      e.target.value === 'WARNING' ? 'Warning' : 'Alert',
                    );
                  }}
                  inputProps={{
                    name: 'label',
                    id: 'type-input-label',
                    SelectDisplayProps: {
                      'data-testid': 'label-select',
                    },
                  }}
                  variant="filled"
                >
                  {Object.keys(NotificationLabel).map(key => (
                    <MenuItem value={key} key={key}>
                      {NotificationLabel[key]}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            {eventLevelThresholdDisabled ? null : (
              <Grid item xs={2} data-testid="notification-tag">
                <NotificationTag type={notificationTagText} />
              </Grid>
            )}
          </Grid>
        )}
        {eventLevelThresholdDisabled ? null : (
          <Grid container>
            <Grid item xs={5}>
              <FormControl
                className={classes.inputField}
                error={!formData.neweventlevel.isValid}
              >
                <InputLabel htmlFor="type-input-eventlevel">
                  Event Level
                </InputLabel>
                <Select
                  value={formData.neweventlevel.value}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    onChangeInput('neweventlevel', e.target.value);
                  }}
                  inputProps={{
                    name: 'eventlevel',
                    id: 'type-input-eventlevel',
                    SelectDisplayProps: {
                      'data-testid': 'eventlevel-select',
                    },
                  }}
                  variant="filled"
                >
                  <MenuItem value="" key="">
                    <em>None</em>
                  </MenuItem>
                  {eventLevels[formData.category.value] &&
                    eventLevels[formData.category.value].map(eventlevel => (
                      <MenuItem value={eventlevel} key={eventlevel}>
                        {eventlevel}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={5}>
              <TextField
                className={classes.inputField}
                inputProps={{ 'data-testid': 'threshold-input' }}
                value={formData.threshold.value}
                label="Threshold"
                error={!formData.threshold.isValid}
                onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                  onChangeInput('threshold', e.target.value);
                }}
                variant="filled"
              />
            </Grid>
            {eventTypeDisabled ? (
              <Grid item xs={2} data-testid="notification-tag">
                <NotificationTag type={notificationTagText} />
              </Grid>
            ) : null}
          </Grid>
        )}
        <Grid container align-items="flex-start">
          <Grid item xs={5}>
            <KeyboardDateTimePicker
              autoOk
              ampm={false}
              error={formData.neweventstart.errors.length > 0}
              helperText={getValidationMessage(formData.neweventstart.errors)}
              label="Start (UTC Time)"
              size="small"
              disableToolbar
              variant="inline"
              format={dateFormat}
              margin="normal"
              inputProps={{ 'data-testid': 'start-date-input' }}
              data-testid="start-date-picker"
              value={formData.neweventstart.value}
              onChange={(newDate: Moment): void => {
                const value = newDate ? newDate.format(dateFormat) : null;
                onChangeInput('neweventstart', value);
              }}
              className={classes.inputField}
              KeyboardButtonProps={{ size: 'small' }}
              keyboardIcon={<CalendarTodayIcon fontSize="small" />}
              inputVariant="filled"
            />
          </Grid>
          <Grid item xs={5}>
            <KeyboardDateTimePicker
              autoOk
              ampm={false}
              error={formData.neweventend.errors.length > 0}
              helperText={getValidationMessage(formData.neweventend.errors)}
              label="End (UTC Time)"
              size="small"
              disableToolbar
              disablePast={!allowEndDateInPast}
              variant="inline"
              format={dateFormat}
              margin="normal"
              inputProps={{ 'data-testid': 'end-date-input' }}
              data-testid="end-date-picker"
              value={formData.neweventend.value}
              onChange={(newDate: Moment): void => {
                const value = newDate ? newDate.format(dateFormat) : null;
                onChangeInput('neweventend', value);
              }}
              className={classes.inputField}
              KeyboardButtonProps={{ size: 'small' }}
              keyboardIcon={<CalendarTodayIcon fontSize="small" />}
              inputVariant="filled"
            />
          </Grid>
          {eventLevelThresholdDisabled ? (
            <Grid item xs={2} data-testid="notification-tag">
              <NotificationTag type={notificationTagText} />
            </Grid>
          ) : null}
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

export default LifeCycleEditOptions;
