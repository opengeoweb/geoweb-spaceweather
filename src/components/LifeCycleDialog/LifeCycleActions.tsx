/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Button, makeStyles, Grid } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles({
  optionButton: {
    marginRight: 10,
  },
});

interface LifeCycleActionsProps {
  actions: string[];
  onAction: (actionType: string) => void;
}

export const LifeCycleActions: React.FC<LifeCycleActionsProps> = ({
  actions,
  onAction,
}: LifeCycleActionsProps) => {
  const classes = useStyles();

  return (
    <Grid
      container
      alignItems="center"
      justify="flex-end"
      data-testid="lifecycle-actions-displaybuttons"
    >
      <Grid item>
        {(actions.includes('updated') || actions.includes('extended')) && (
          <Button
            data-testid="updateextend"
            color="secondary"
            onClick={(): void => onAction('Updateextend')}
            className={classes.optionButton}
            startIcon={<EditIcon />}
          >
            Update/Extend
          </Button>
        )}
      </Grid>
      <Grid item>
        {actions.includes('summarised') && (
          <Button
            data-testid="summarise"
            variant="contained"
            color="secondary"
            onClick={(): void => onAction('Summarise')}
            className={classes.optionButton}
          >
            Summarise
          </Button>
        )}
      </Grid>
      <Grid item>
        {actions.includes('cancelled') && (
          <Button
            data-testid="cancel"
            variant="contained"
            color="secondary"
            onClick={(): void => onAction('Cancel')}
            className={classes.optionButton}
          >
            Cancel
          </Button>
        )}
      </Grid>
    </Grid>
  );
};

export default LifeCycleActions;
