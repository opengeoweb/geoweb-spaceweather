/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Button,
  makeStyles,
  TextField,
  Grid,
  Backdrop,
  CircularProgress,
  Typography,
} from '@material-ui/core';

import { LifeCycleEditOptions } from './LifeCycleEditOptions';
import { SWEvent, SWNotification } from '../../types';
import {
  constructOutgoingNotification,
  constructBaseNotification,
  constructBaseNotificationWithValidation,
  validateField,
  SWNotificationWithValidation,
  deconstructBaseNotificationWithValidation,
  countFieldsWithError,
} from './utils';
import { useApiContext } from '../ApiContext/ApiContext';

const useStyles = makeStyles({
  editor: {
    width: '100%',
    backgroundColor: 'rgba(0, 117, 169, 0.05)',
  },
  config: {
    width: '1000px',
  },
  optionButton: {
    marginRight: 10,
  },
  backdrop: {
    zIndex: 1001,
    color: '#fff',
  },
  errorMessage: {
    color: 'red',
  },
});

interface DiscardRequest {
  notificationid: string;
  eventid: string;
}

interface LifeCycleEditProps {
  toggleDialogOpen: Function;
  notificationTag?: string;
  eventTypeDisabled?: boolean;
  baseNotificationData?: SWEvent;
  baseNotificationType: string;
  allowEndDateInPast: boolean;
  requestPromiseStore?: (formData: SWNotification) => Promise<void>;
  requestPromiseDiscardDraft?: (DiscardRequest) => Promise<void>;
  onFormChange?: (hasChanged: boolean) => void;
}

function useFormChanged<T>(initState: T): [boolean] {
  const [initialState] = React.useState<T>(initState);
  const [isChanged, setIsChanged] = React.useState(false);
  React.useEffect(() => {
    const hasChanged = Object.keys(initialState).reduce(
      (total, key) =>
        initialState[key].value !== initState[key].value ? true : total,
      false,
    );

    setIsChanged(hasChanged);
  }, [initState, initialState]);

  return [isChanged];
}

function usePreventBrowserClose(shouldPreventClose: boolean): void {
  const showPrompt = (event: Event): void => {
    // @ts-ignore
    // eslint-disable-next-line no-param-reassign
    event.returnValue = 'Changes that you made may not be saved.'; // changes are your browser have default text to overrule this
  };

  React.useEffect(() => {
    if (shouldPreventClose) {
      window.addEventListener('beforeunload', showPrompt);
    } else {
      window.removeEventListener('beforeunload', showPrompt);
    }
    return (): void => {
      window.removeEventListener('beforeunload', showPrompt);
    };
  }, [shouldPreventClose]);
}

export const LifeCycleEdit: React.FC<LifeCycleEditProps> = ({
  toggleDialogOpen,
  notificationTag = 'Alert',
  eventTypeDisabled = false,
  baseNotificationData = null,
  baseNotificationType,
  allowEndDateInPast,
  requestPromiseStore,
  requestPromiseDiscardDraft,
  onFormChange = (): void => null,
}: LifeCycleEditProps) => {
  const classes = useStyles();

  const { api } = useApiContext();

  const requestStore = requestPromiseStore || api.issueNotification;
  const requestDiscardDraft =
    requestPromiseDiscardDraft || api.discardDraftNotification;

  const [errorStoreMessage, setErrorStoreMessage] = React.useState(false);
  const [showLoader, setLoader] = React.useState(false);
  const [formData, setFormData] = React.useState<SWNotificationWithValidation>(
    constructBaseNotificationWithValidation(
      constructBaseNotification(baseNotificationData, baseNotificationType),
    ),
  );
  const [hasFormChanged] = useFormChanged(formData);
  usePreventBrowserClose(hasFormChanged);

  React.useEffect(() => {
    onFormChange(hasFormChanged);
  }, [hasFormChanged, onFormChange]);

  const handleDiscard = (): void => {
    // If a draft is discarded  - remove from database
    if (formData.draft.value && formData.notificationid.value) {
      setLoader(true);
      setErrorStoreMessage(false);
      requestDiscardDraft({
        notificationid: formData.notificationid.value,
        eventid: formData.eventid.value,
      })
        .then(() => {
          toggleDialogOpen(true);
          setLoader(false);
        })
        .catch(() => {
          setErrorStoreMessage(true);
          setLoader(false);
        });
    } else {
      toggleDialogOpen();
    }
  };

  const validateForm = (): boolean => {
    let hasError = false;
    const newState = Object.keys(formData).reduce((list, key) => {
      const formField = formData[key];
      const errors = validateField(
        formField.value,
        formField.validations,
        formData,
      );

      if (errors.length) {
        hasError = true;
      }

      return {
        ...list,
        [key]: {
          ...formField,
          errors,
          isValid: errors.length === 0,
        },
      };
    }, {} as SWNotificationWithValidation);

    if (hasError) {
      setFormData(newState);
    }

    return hasError;
  };

  const handleStoreNotification = (isDraft = false): void => {
    if (!isDraft) {
      const hasErrors = validateForm();
      if (hasErrors) {
        return;
      }
    }

    setLoader(true);
    setErrorStoreMessage(false);
    const notificationData = constructOutgoingNotification(
      deconstructBaseNotificationWithValidation(formData),
      notificationTag,
      isDraft,
    );
    requestStore(notificationData)
      .then(() => {
        setLoader(false);
        toggleDialogOpen(true);
      })
      .catch(() => {
        setErrorStoreMessage(true);
        setLoader(false);
      });
  };

  React.useEffect(
    () => {
      // unfortunately this is an exception of the validations, when start date changes also check the endDate for validity
      const errors = validateField(
        formData.neweventend.value,
        ['afterStartDate'],
        formData,
      );
      setFormData({
        ...formData,
        neweventend: {
          ...formData.neweventend,
          errors,
          isValid: errors.length === 0,
        },
      });
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formData.neweventstart.value],
  );

  const onUpdateField = (field): void => {
    setFormData({
      ...formData,
      ...field,
    });
  };

  const onChangeMessage = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    const { value } = event.target;
    const field = 'message';

    const errors = validateField(value, formData[field].validations, formData);
    const updatedField = {
      [field]: {
        ...formData[field],
        value,
        errors,
        isValid: !errors.length,
      },
    };
    onUpdateField(updatedField);
  };

  const [requiredFields, errorFields] = countFieldsWithError(formData);

  return (
    <Grid container spacing={1} data-testid="edit-lifecycle">
      <Grid item xs={12}>
        <LifeCycleEditOptions
          formData={formData}
          notificationTag={notificationTag}
          eventTypeDisabled={eventTypeDisabled}
          allowEndDateInPast={allowEndDateInPast}
          onUpdateFormField={onUpdateField}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          inputProps={{ 'data-testid': 'notification-text' }}
          value={formData.message.value}
          multiline
          rows={20}
          variant="outlined"
          className={classes.editor}
          error={!formData.message.isValid}
          onChange={onChangeMessage}
          placeholder="Event description"
        />
      </Grid>
      <Grid item xs={12} container justify="flex-end">
        {errorStoreMessage && (
          <Typography variant="body2" className={classes.errorMessage}>
            An error has occurred, please try again
          </Typography>
        )}
      </Grid>
      <Grid
        item
        data-testid="new-notification-options"
        container
        xs={12}
        alignItems="center"
        justify="flex-end"
      >
        <Grid item>
          <Button
            data-testid="discard"
            color="secondary"
            onClick={handleDiscard}
            className={classes.optionButton}
          >
            Discard
          </Button>
        </Grid>
        <Grid item>
          <Button
            data-testid="draft"
            variant="outlined"
            color="secondary"
            className={classes.optionButton}
            disabled={!!errorFields.length}
            onClick={(): void => {
              handleStoreNotification(true);
            }}
          >
            Save as draft
          </Button>
        </Grid>
        <Grid item>
          <Button
            data-testid="issue"
            variant="contained"
            color="secondary"
            className={classes.optionButton}
            disabled={!!errorFields.length || !!requiredFields.length}
            onClick={(): void => {
              handleStoreNotification();
            }}
          >
            Issue
          </Button>
        </Grid>
      </Grid>
      <Backdrop
        data-testid="loader"
        open={showLoader}
        className={classes.backdrop}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </Grid>
  );
};

export default LifeCycleEdit;
