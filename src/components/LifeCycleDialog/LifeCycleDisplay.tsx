/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Grid, ListItem } from '@material-ui/core';
import { Alert, AlertTitle, Skeleton } from '@material-ui/lab';
import { SubdirectoryArrowRight } from '@material-ui/icons';

import { useApi } from '../../utils/hooks';
import LifeCycleColumn from './LifeCycleColumn';
import LifeCycleActions from './LifeCycleActions';
import LifeCycleEdit from './LifeCycleEdit';
import { getNotificationTagContent } from '../Notifications/NotificationTag';
import { SWNotification, SWEvent } from '../../types';

const getNewInternalNotificationTag = (
  actionMode: string,
  label: string,
): string => {
  if (actionMode === 'Summarise') {
    return 'Summary';
  }
  if (actionMode === 'Cancel') {
    return 'Cancelled';
  }
  if (label === 'WARNING') {
    return 'Warning';
  }
  return 'Alert';
};

interface LifeCycleDisplayProps {
  eventId: string;
  toggleDialogOpen: Function;
  requestPromiseStore?: (formData: SWNotification) => Promise<void>;
  requestPromiseEvent?: (eventId: string) => Promise<{ data: SWEvent }>;
  requestPromiseDiscardDraft?: (notificationId: string) => Promise<void>;
  onFormChange?: (hasChanged: boolean) => void;
}

export const LifeCycleDisplay: React.FC<LifeCycleDisplayProps> = ({
  eventId,
  toggleDialogOpen,
  requestPromiseEvent,
  requestPromiseStore,
  requestPromiseDiscardDraft,
  onFormChange,
}: LifeCycleDisplayProps) => {
  const { isLoading, error, result } = useApi(requestPromiseEvent, eventId);

  const [actionMode, setActionMode] = React.useState('none');

  const onAction = (actionType): void => {
    setActionMode(actionType);
  };

  const draft =
    result &&
    result.lifecycles.internalprovider &&
    result.lifecycles.internalprovider.draft === true
      ? result.lifecycles.internalprovider.notifications[
          result.lifecycles.internalprovider.notifications.length - 1
        ]
      : false;

  const isInternalProviderLifeCycle =
    result &&
    result.lifecycles.internalprovider !== undefined &&
    result.lifecycles.internalprovider.eventid;

  return (
    <>
      {result && (
        <Grid container data-testid="display-lifecycle">
          {result.lifecycles.externalprovider &&
            result.lifecycles.externalprovider.firstissuetime && (
              <Grid item xs={6} data-testid="display-lifecycle-externalcolumn">
                <LifeCycleColumn
                  lifeCycle={result.lifecycles.externalprovider}
                />
              </Grid>
            )}
          <Grid
            item
            xs={result.lifecycles.externalprovider ? 6 : 12}
            data-testid="display-lifecycle-internalcolumn"
          >
            {isInternalProviderLifeCycle && (
              <LifeCycleColumn lifeCycle={result.lifecycles.internalprovider} />
            )}
            <ListItem>
              {// internalprovider notification exists already - no action chosen - display buttons
              isInternalProviderLifeCycle &&
                actionMode === 'none' &&
                !draft && (
                  <LifeCycleActions
                    actions={result.lifecycles.internalprovider.canbe}
                    onAction={onAction}
                  />
                )}
              {// No internalprovider notification issued yet - incoming externalprovider notification exists - base category, type and start/end time of event
              result !== undefined &&
                !draft &&
                !isInternalProviderLifeCycle &&
                actionMode === 'none' && (
                  <LifeCycleEdit
                    toggleDialogOpen={toggleDialogOpen}
                    notificationTag={getNotificationTagContent(
                      result.lifecycles.externalprovider.label,
                    )}
                    eventTypeDisabled
                    baseNotificationData={result}
                    baseNotificationType="externalprovider"
                    allowEndDateInPast={false}
                    requestPromiseStore={requestPromiseStore}
                    requestPromiseDiscardDraft={requestPromiseDiscardDraft}
                    onFormChange={onFormChange}
                  />
                )}

              {// internalprovider notification exists already (no draft) - we are performing an action after pressing one of the buttons - base category, type and start/end time off event
              isInternalProviderLifeCycle && !draft && actionMode !== 'none' && (
                <Grid
                  container
                  spacing={1}
                  data-testid="lifecycle-display-internaleditaction"
                >
                  <Grid item xs={12}>
                    <SubdirectoryArrowRight style={{ color: '#0075a9' }} />
                  </Grid>
                  <Grid item xs={12}>
                    <LifeCycleEdit
                      toggleDialogOpen={toggleDialogOpen}
                      notificationTag={getNewInternalNotificationTag(
                        actionMode,
                        result.lifecycles.internalprovider.label,
                      )}
                      eventTypeDisabled
                      baseNotificationData={result}
                      baseNotificationType="internalprovider"
                      allowEndDateInPast={
                        actionMode === 'Summarise' || actionMode === 'Cancel'
                      }
                      requestPromiseStore={requestPromiseStore}
                      requestPromiseDiscardDraft={requestPromiseDiscardDraft}
                      onFormChange={onFormChange}
                    />
                  </Grid>
                </Grid>
              )}

              {// internalprovider draft notification exists - base category, type and start/end time off event filled in from draft

              isInternalProviderLifeCycle && draft && (
                <Grid
                  container
                  spacing={1}
                  data-testid="lifecycle-display-internaldraftedit"
                >
                  {result.lifecycles.internalprovider.notifications.length >
                    1 && (
                    <Grid item xs={12}>
                      <SubdirectoryArrowRight style={{ color: '#0075a9' }} />
                    </Grid>
                  )}
                  <Grid item xs={12}>
                    <LifeCycleEdit
                      toggleDialogOpen={toggleDialogOpen}
                      baseNotificationData={result}
                      baseNotificationType="draft"
                      notificationTag={getNotificationTagContent(
                        draft.label,
                        draft.changestateto,
                      )}
                      eventTypeDisabled
                      allowEndDateInPast={draft.changestateto === 'ended'}
                      requestPromiseStore={requestPromiseStore}
                      requestPromiseDiscardDraft={requestPromiseDiscardDraft}
                      onFormChange={onFormChange}
                    />
                  </Grid>
                </Grid>
              )}
            </ListItem>
          </Grid>
        </Grid>
      )}
      {isLoading && <Skeleton variant="rect" height={500} width="100%" />}
      {error && (
        <Alert severity="error">
          <AlertTitle>{error.message}</AlertTitle>
        </Alert>
      )}
    </>
  );
};

export default LifeCycleDisplay;
