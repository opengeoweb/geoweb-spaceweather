/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import LifeCycleEdit from './LifeCycleEdit';
import { mockEvent } from '../../utils/__mocks__/hooks';
import { mockDraftEvent } from '../../utils/__mocks__/api';
import { EventCategory } from '../../types';
import { ApiProvider } from '../ApiContext/ApiContext';

jest.mock('../../utils/api');

describe('src/components/LifeCycleDialog/LifeCycleEdit', () => {
  it('should disable the draft and issue buttons when end date has an error', () => {
    const start = '2020-07-01 10:00';
    const end = '2020-07-01';
    const event = mockEvent;
    event.lifecycles.internalprovider.eventstart = start;
    event.lifecycles.internalprovider.eventend = end;

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: event,
      allowEndDateInPast: false,
    };

    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );

    fireEvent.change(getByTestId('end-date-input'), { target: { value: end } });
    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).toContain('Mui-error');
    expect(getByTestId('draft').getAttribute('class')).toContain(
      'Mui-disabled',
    );
    expect(getByTestId('issue').getAttribute('class')).toContain(
      'Mui-disabled',
    );
    expect(getByTestId('discard').getAttribute('class')).not.toContain(
      'Mui-disabled',
    );
  });

  it('should handle discarding a new notification', () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null,
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );
    fireEvent.click(getByTestId('discard'));

    expect(props.toggleDialogOpen).toHaveBeenCalled();
    // loader should not be visible
    expect(getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 0;',
    );
  });

  it('should handle saving a new notification as draft', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null,
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );

    const messageInput = getByTestId('notification-text');
    const message = 'Solar activity was very low over the past 24 hours.';
    fireEvent.change(messageInput, {
      target: { value: message },
    });

    fireEvent.click(getByTestId('draft'));

    // loader should be visible
    expect(getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 1;',
    );
    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );
    expect(props.toggleDialogOpen).toHaveBeenCalled();
  });

  it('should show an error message when issue new notification fails', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null,
      allowEndDateInPast: true,
    };
    const { getByTestId, getByText, findByText } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );

    // enter eventlevel
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelR = await findByText('R1');
    fireEvent.click(menuItemLevelR);

    // enter threshold
    fireEvent.change(getByTestId('threshold-input'), {
      target: { value: 'Dummy threshold text' },
    });

    // enter notificationtext
    fireEvent.change(getByTestId('notification-text'), {
      target: { value: 'Dummy notification text' },
    });

    fireEvent.click(getByTestId('issue'));

    // loader should be visible
    expect(getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 1;',
    );
    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );
    expect(props.toggleDialogOpen).not.toHaveBeenCalled();
    expect(getByText('An error has occurred, please try again')).toBeTruthy();
  });

  it('should handle discarding a draft notification', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'draft',
      baseNotificationData: mockDraftEvent,
      allowEndDateInPast: true,
    };
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );

    fireEvent.click(getByTestId('discard'));

    // loader should be visible
    expect(getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 1;',
    );
    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );

    expect(props.toggleDialogOpen).toHaveBeenCalled();
  });

  it('should update the event level options when changing the event category', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: mockEvent,
      allowEndDateInPast: true,
    };
    const { getByTestId, findByText } = render(
      <ApiProvider baseURL="local.test">
        <LifeCycleEdit {...props} />
      </ApiProvider>,
    );

    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelR = await findByText('R1');
    fireEvent.click(menuItemLevelR);

    fireEvent.mouseDown(getByTestId('category-select'));
    const menuItemCategory = await findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(menuItemCategory);

    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelG = await findByText('G1');
    fireEvent.click(menuItemLevelG);
  });
});
