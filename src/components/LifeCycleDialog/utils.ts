/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import moment from 'moment';

import {
  SWNotification,
  SWEvent,
  EventCategory,
  NotificationLabel,
} from '../../types';

export const constructBaseNotification = (
  event: SWEvent | null,
  mode = 'new',
): SWNotification => {
  switch (mode) {
    case 'draft': {
      const draftEvent = event.lifecycles.internalprovider.notifications.find(
        notification => notification.draft,
      );
      const neweventstart = moment
        .utc(draftEvent.neweventstart)
        .format('YYYY-MM-DD HH:mm');
      const neweventend =
        draftEvent.neweventend &&
        draftEvent.neweventend !== null &&
        draftEvent.neweventend !== ''
          ? moment.utc(draftEvent.neweventend).format('YYYY-MM-DD HH:mm')
          : '';
      return {
        ...draftEvent,
        eventid: event.eventid,
        neweventstart,
        neweventend,
        neweventlevel:
          draftEvent.neweventlevel !== null ? draftEvent.neweventlevel : '',
      };
    }
    case 'internalprovider': {
      const lifeCycle = event.lifecycles.internalprovider;
      const neweventstart = moment
        .utc(lifeCycle.eventstart)
        .format('YYYY-MM-DD HH:mm');
      const neweventend =
        lifeCycle.eventend &&
        lifeCycle.eventend !== null &&
        lifeCycle.eventend !== ''
          ? moment.utc(lifeCycle.eventend).format('YYYY-MM-DD HH:mm')
          : null;

      // Get latest message
      const message =
        lifeCycle.notifications.length &&
        lifeCycle.notifications[lifeCycle.notifications.length - 1].message
          ? lifeCycle.notifications[lifeCycle.notifications.length - 1].message
          : '';

      return {
        ...(lifeCycle.eventid !== null && { eventid: lifeCycle.eventid }),
        category: event.category,
        label: lifeCycle.label,
        changestateto: '',
        draft: false,
        neweventstart,
        neweventend,
        neweventlevel:
          lifeCycle.eventlevel && lifeCycle.eventlevel !== null
            ? lifeCycle.eventlevel
            : '',
        threshold: lifeCycle.threshold,
        message,
        originator: 'KNMI',
      };
    }
    case 'externalprovider': {
      const lifeCycle = event.lifecycles.externalprovider;
      const neweventstart = moment
        .utc(lifeCycle.eventstart)
        .format('YYYY-MM-DD HH:mm');
      const neweventend =
        lifeCycle.eventend &&
        lifeCycle.eventend !== null &&
        lifeCycle.eventend !== ''
          ? moment.utc(lifeCycle.eventend).format('YYYY-MM-DD HH:mm')
          : null;

      return {
        ...(lifeCycle.eventid !== null && { eventid: lifeCycle.eventid }),
        category: event.category,
        label: lifeCycle.label,
        changestateto: lifeCycle.state,
        draft: false,
        neweventstart,
        neweventend,
        neweventlevel:
          lifeCycle.eventlevel && lifeCycle.eventlevel !== null
            ? lifeCycle.eventlevel
            : '',
        threshold: lifeCycle.threshold,
        message: '',
        originator: 'KNMI',
      };
    }
    case 'new':
    default: {
      // Defaults for the date
      const now = moment.utc().format('YYYY-MM-DD HH:mm');
      const tomorrow = moment
        .utc()
        .add(24, 'hours')
        .format('YYYY-MM-DD HH:mm');
      return {
        category: Object.keys(EventCategory)[0],
        label: Object.keys(NotificationLabel)[0],
        draft: false,
        neweventstart: now,
        neweventend: tomorrow,
        changestateto: 'issued',
        threshold: '',
        neweventlevel: '',
        message: '',
        originator: 'KNMI',
      };
    }
  }
};
export interface ValidationField {
  value: string;
  errors: string[];
  isValid: boolean;
  validations: string[];
}

type ValidationType =
  | 'required'
  | 'validDate'
  | 'afterStartDate'
  | 'dateInFuture';

export interface SWNotificationWithValidation {
  label: ValidationField;
  changestateto: ValidationField;
  category: ValidationField;
  neweventlevel?: ValidationField;
  neweventstart: ValidationField;
  neweventend: ValidationField;
  threshold: ValidationField;
  draft?: ValidationField;
  message: ValidationField;
  srcEventId?: ValidationField;
  notificationid?: ValidationField;
  eventid?: ValidationField;
  issuetime?: ValidationField;
  originator?: ValidationField;
}

const getValidationByKey = (key: string): ValidationType[] => {
  switch (key) {
    case 'category':
    case 'label':
    case 'eventlevel':
    case 'neweventlevel':
    case 'threshold':
    case 'message':
      return ['required'];
    case 'neweventstart':
      return ['required', 'validDate'];
    case 'neweventend':
      return ['validDate', 'afterStartDate', 'dateInFuture'];
    default:
      return [];
  }
};
const dateFormat = 'YYYY-MM-DD HH:mm';

export const MESSAGE_END_DATE_IN_FUTURE = 'End date should be in the future';
export const MESSAGE_END_DATE_AFTER_START_DATE =
  'End date should be after start date';
export const MESSAGE_INVALID_FORMAT = `Date format should be ${dateFormat}`;
export const MESSAGE_REQUIRED = 'This field is required';

export const getValidationMessage = (
  errors: ValidationType | string[],
): string => {
  switch (errors[0]) {
    case 'required':
      return MESSAGE_REQUIRED;
    case 'validDate':
      return MESSAGE_INVALID_FORMAT;
    case 'afterStartDate':
      return MESSAGE_END_DATE_AFTER_START_DATE;
    case 'dateInFuture':
      return MESSAGE_END_DATE_IN_FUTURE;
    default:
      return null;
  }
};

// TODO: this is a wrapper function, to limit the loops better to combine them
export const constructBaseNotificationWithValidation = (
  notification: SWNotification,
): SWNotificationWithValidation => {
  return Object.keys(notification).reduce((list, key) => {
    const validations = getValidationByKey(key);
    const value = notification[key];

    return {
      ...list,
      [key]: {
        value,
        isValid: true,
        errors: [],
        validations,
      },
    };
  }, {}) as SWNotificationWithValidation;
};

export const deconstructBaseNotificationWithValidation = (
  notificationWithValidation: SWNotificationWithValidation,
): SWNotification =>
  Object.keys(notificationWithValidation).reduce(
    (list, key) => ({
      ...list,
      [key]: notificationWithValidation[key].value,
    }),
    {} as SWNotification,
  );

export const validateField = (
  value,
  validations: string[],
  formData: SWNotificationWithValidation,
): ValidationType[] => {
  const errors = validations.reduce((total, validation) => {
    if (total.length) {
      return total;
    }
    let isValid = true;
    if (validation === 'required') {
      isValid = value && value.length > 0;
    }

    if (validation === 'validDate') {
      isValid = value ? moment(value, dateFormat, true).isValid() : true;
    }
    if (validation === 'dateInFuture') {
      isValid = value ? moment.utc(value) >= moment.utc() : true;
    }
    // TODO: must be better way to send the whole form as param
    if (validation === 'afterStartDate') {
      if (
        moment(formData.neweventstart.value, dateFormat, true).isValid() &&
        value
      ) {
        isValid = value > formData.neweventstart.value;
      }
    }
    return !isValid ? total.concat(validation) : total;
  }, []);
  return errors;
};

export const constructOutgoingNotification = (
  notification: SWNotification,
  newState,
  draft = false,
): SWNotification => {
  const changestateto =
    newState === 'Alert' || newState === 'Warning' ? 'issued' : 'ended';
  const neweventend =
    notification.neweventend &&
    notification.neweventend !== '' &&
    notification.neweventend !== null
      ? moment.utc(notification.neweventend).format()
      : '';

  return {
    ...notification,
    neweventstart: moment.utc(notification.neweventstart).format(),
    neweventend,
    draft,
    changestateto,
  };
};

export const countFieldsWithError = (
  formData: SWNotificationWithValidation,
): [ValidationType[], ValidationType[]] =>
  Object.keys(formData).reduce(
    (lists, key) => {
      const [requiredErrors, otherError] = lists;
      formData[key].errors.forEach(error => {
        if (error === 'required') {
          requiredErrors.push(error);
        } else {
          otherError.push(error);
        }
      });

      return lists;
    },
    [[], []],
  );
