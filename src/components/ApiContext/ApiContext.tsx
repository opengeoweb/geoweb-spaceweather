/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { createApi } from '../../utils/api';

const ApiContext = React.createContext({
  api: null,
});

interface ApiContextState {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  api: any;
}

interface ApiContextProps {
  children: React.ReactNode;
  baseURL: string;
}

export const ApiProvider: React.FC<ApiContextProps> = ({
  children,
  baseURL,
}: ApiContextProps) => {
  const api = createApi(baseURL);
  return (
    <ApiContext.Provider
      value={{
        api,
      }}
    >
      {children}
    </ApiContext.Provider>
  );
};

export const useApiContext = (): Partial<ApiContextState> => {
  const context = React.useContext(ApiContext);
  return context;
};

export default ApiContext;
