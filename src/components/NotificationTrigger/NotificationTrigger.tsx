/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { Alert } from '@material-ui/lab';
import { useNotificationTriggerContext } from './NotificationTriggerContext';

export const NotificationTrigger: React.FC = () => {
  const { notificationTriggers, error } = useNotificationTriggerContext();

  if (error) {
    return (
      <Alert severity="error">
        New notification trigger retrieval: {error.message}
      </Alert>
    );
  }
  return notificationTriggers && notificationTriggers.length > 0 ? (
    <Alert severity="warning" icon={<ErrorOutlineIcon fontSize="inherit" />}>
      There {notificationTriggers.length === 1 ? `is ` : `are `}
      {notificationTriggers.length} unhandled notification{' '}
      {notificationTriggers.length === 1 ? `trigger` : `triggers`}.
    </Alert>
  ) : null;
};

export default NotificationTrigger;
