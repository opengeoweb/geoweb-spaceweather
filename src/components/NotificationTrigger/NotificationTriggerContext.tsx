/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { useApi } from '../../utils/hooks';
import { SWEvent } from '../../types';
import { useApiContext } from '../ApiContext/ApiContext';

const NotificationTriggerContext = React.createContext({
  notificationTriggers: null,
  error: null,
  onFetchNewNotificationTriggerData: () => {
    // do nothing
  },
});

interface NotificationContextState {
  notificationTriggers: SWEvent[];
  error: Error;
  onFetchNewNotificationTriggerData: () => void;
}

interface NotificationContextProps {
  children: React.ReactNode;
  requestPromiseNotificationTrigger?: () => Promise<{ data: SWEvent[] }>;
}

export const NotificationTriggerProvider: React.FC<NotificationContextProps> = ({
  children,
  requestPromiseNotificationTrigger,
}: NotificationContextProps) => {
  const { api } = useApiContext();

  const interval = React.useRef(null);

  const { error, result: notificationTriggers, fetchApiData } = useApi(
    requestPromiseNotificationTrigger || api.getNewNotifications,
  );

  React.useEffect(() => {
    const pollingInterval = 60000;
    if (notificationTriggers || error) {
      interval.current = setInterval(() => {
        fetchApiData(null);
      }, pollingInterval);
    }
    return (): void => {
      if (interval.current) {
        clearInterval(interval.current);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notificationTriggers, error]);

  const onFetchNewNotificationTriggerData = (): void => {
    if (interval.current) {
      clearInterval(interval.current);
    }
    fetchApiData(null);
  };

  return (
    <NotificationTriggerContext.Provider
      value={{
        notificationTriggers,
        error,
        onFetchNewNotificationTriggerData,
      }}
    >
      {children}
    </NotificationTriggerContext.Provider>
  );
};

export const useNotificationTriggerContext = (): Partial<NotificationContextState> => {
  const context = React.useContext(NotificationTriggerContext);
  return context;
};

export default NotificationTriggerContext;
