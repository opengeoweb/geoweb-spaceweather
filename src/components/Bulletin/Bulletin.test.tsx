/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import Bulletin from './Bulletin';
import { ApiProvider } from '../ApiContext/ApiContext';

jest.mock('../../utils/hooks');

describe('src/components/Bulletin/Bulletin', () => {
  it('should show the history dialog', async () => {
    const { getByTestId, queryByTestId } = render(
      <ApiProvider baseURL="local.test">
        <Bulletin />
      </ApiProvider>,
    );

    fireEvent.click(getByTestId('history'));
    expect(getByTestId('history-dialog')).toBeTruthy();

    // When closing the dialog - should be hidden
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => expect(queryByTestId('history-dialog')).toBeFalsy());
  });

  it('should show the bulletin viewer once the latest bulletin has been retrieved', async () => {
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <Bulletin />
      </ApiProvider>,
    );

    await waitFor(() => expect(getByTestId('bulletinviewer')).toBeTruthy());
  });

  it('should show the bulletin tabs and have the technical forecast selected as default', async () => {
    const { getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <Bulletin />
      </ApiProvider>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('technical').classList).toContain('Mui-selected');
    expect(getByTestId('plain').classList).not.toContain('Mui-selected');
  });
});
