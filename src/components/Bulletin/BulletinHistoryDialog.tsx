/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Grid, makeStyles } from '@material-ui/core';
import { Skeleton, Alert, AlertTitle } from '@material-ui/lab';

import { BulletinType } from './Bulletin';
import { BulletinViewer } from './BulletinViewer';
import { ContentDialog } from '../ContentDialog';
import { BulletinHistoryList } from './BulletinHistoryList';
import { useApi } from '../../utils/hooks';

import { BulletinTabs } from './BulletinTabs';
import { useApiContext } from '../ApiContext/ApiContext';

const useStyles = makeStyles({
  viewer: {
    padding: '10px 10px 0px 10px',
  },
});

interface BulletinHistoryDialogProps {
  open: boolean;
  toggleStatus: Function;
}

const BulletinHistoryDialog: React.FC<BulletinHistoryDialogProps> = ({
  open,
  toggleStatus,
}: BulletinHistoryDialogProps) => {
  const [bulletinType, setBulletinType] = React.useState(
    BulletinType.technical,
  );
  const [selectedBulletin, setSelectedBulletin] = React.useState('');
  const { api } = useApiContext();

  const {
    isLoading,
    error,
    result: bulletinHistoryList,
    fetchApiData,
  } = useApi(api.getBulletinHistory);

  React.useEffect(() => {
    if (
      bulletinHistoryList &&
      bulletinHistoryList.length &&
      !selectedBulletin
    ) {
      setSelectedBulletin(bulletinHistoryList[0].bulletin_id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bulletinHistoryList]);

  React.useEffect(() => {
    if (open) {
      fetchApiData(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);
  const classes = useStyles();
  return (
    <ContentDialog
      open={open}
      toggleStatus={toggleStatus}
      title="Forecast History"
      data-testid="history-dialog"
      fullWidth
      disableEscapeKeyDown
    >
      <Grid container>
        <Grid item xs={2}>
          {bulletinHistoryList && (
            <BulletinHistoryList
              selectedBulletin={selectedBulletin}
              bulletinHistoryList={bulletinHistoryList}
              onSelectBulletin={setSelectedBulletin}
            />
          )}
          {isLoading && <Skeleton variant="rect" height={558} width="90%" />}
          {error && (
            <Alert severity="error">
              <AlertTitle>{error.message}</AlertTitle>
            </Alert>
          )}
        </Grid>
        <Grid item xs={10}>
          <Grid container direction="column" alignItems="stretch" spacing={1}>
            <BulletinTabs
              selected={bulletinType}
              onSetBulletinType={setBulletinType}
            />
            <Grid item>
              <div className={classes.viewer}>
                {bulletinHistoryList && selectedBulletin && (
                  <BulletinViewer
                    bulletinType={bulletinType}
                    smallContent={false}
                    bulletin={bulletinHistoryList.find(
                      bulletin => bulletin.bulletin_id === selectedBulletin,
                    )}
                  />
                )}
              </div>
              {isLoading && (
                <Skeleton variant="rect" height={500} width="100%" />
              )}
              {error && (
                <Alert severity="error">
                  <AlertTitle>{error.message}</AlertTitle>
                </Alert>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </ContentDialog>
  );
};

export default BulletinHistoryDialog;
