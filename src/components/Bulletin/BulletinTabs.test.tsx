/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { BulletinTabs } from './BulletinTabs';
import { BulletinType } from './Bulletin';

describe('src/components/Bulletin/BulletinHistoryDialog', () => {
  it('should have selected technical tab if that is the one selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    const { getByTestId } = render(<BulletinTabs {...props} />);
    expect(getByTestId('technical').getAttribute('class')).toContain(
      'Mui-selected',
    );
  });

  it('update to plain if plain tab selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    const { getByTestId } = render(<BulletinTabs {...props} />);

    fireEvent.click(getByTestId('plain'));

    expect(props.onSetBulletinType).toHaveBeenCalledWith('PLAIN');
  });

  it('updates to technical if technical tab selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    const { getByTestId } = render(<BulletinTabs {...props} />);

    fireEvent.click(getByTestId('plain'));

    expect(props.onSetBulletinType).toHaveBeenCalledWith('PLAIN');
    fireEvent.click(getByTestId('technical'));

    expect(props.onSetBulletinType).toHaveBeenCalledWith('TECHNICAL');
  });
});
