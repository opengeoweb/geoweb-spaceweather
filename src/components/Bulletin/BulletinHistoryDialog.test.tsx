/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { mockBulletinHistory } from '../../utils/__mocks__/hooks';
import { ApiProvider } from '../ApiContext/ApiContext';

jest.mock('../../utils/hooks');

describe('src/components/Bulletin/BulletinHistoryDialog', () => {
  it('should update the selected bulletin when clicking an item in the history list', () => {
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    const { getAllByTestId, getByTestId } = render(
      <ApiProvider baseURL="local.test">
        <BulletinHistoryDialog {...props} />
      </ApiProvider>,
    );

    // check first item is displayed
    expect(getByTestId('bulletinContent').textContent).toContain(
      mockBulletinHistory.result[0].message.overview.content,
    );

    // click second item
    const historyItems = getAllByTestId('historyItem');
    fireEvent.click(historyItems[1]);

    // check second item is displayed
    expect(getByTestId('bulletinContent').textContent).toContain(
      mockBulletinHistory.result[1].message.overview.content,
    );
  });
});
