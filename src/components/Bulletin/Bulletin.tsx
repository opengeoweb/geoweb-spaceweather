/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Grid,
  makeStyles,
  Typography,
  Card,
  CardContent,
} from '@material-ui/core';
import { Skeleton, Alert, AlertTitle } from '@material-ui/lab';

import { BulletinControls } from './BulletinControls';
import { BulletinViewer } from './BulletinViewer';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { useApi } from '../../utils/hooks';
import { BulletinTabs } from './BulletinTabs';
import { useApiContext } from '../ApiContext/ApiContext';

export enum BulletinType {
  technical = 'TECHNICAL',
  plain = 'PLAIN',
}

const maxHeight = '22vh';

const useStyles = makeStyles({
  BulletinHeader: {
    textAlign: 'center',
  },
  card: { boxShadow: 'inset 0 0 2px 0 rgba(0, 0, 0, 0.5)' },
  cardContent: { height: maxHeight, minHeight: '175px' },
});

/**
 * Bulletin
 * View the latest bulletin from Met Office, and access the history list
 *
 * @example
 * ``` <Bulletin /> ```
 */
const BulletinComponent: React.FC = () => {
  const classes = useStyles();
  const interval = React.useRef(null);
  const { api } = useApiContext();

  const [bulletinType, setBulletinType] = React.useState(
    BulletinType.technical,
  );
  const [bulletinHistoryOpen, setBulletinHistoryOpen] = React.useState(false);
  const toggleBulletinHistory = (): void => {
    setBulletinHistoryOpen(!bulletinHistoryOpen);
  };

  const { isLoading, error, result, fetchApiData } = useApi(api.getBulletin);

  React.useEffect(() => {
    const pollingInterval = 300000;
    if (result || error) {
      interval.current = setInterval(() => {
        fetchApiData(null);
      }, pollingInterval);
    }

    return (): void => {
      clearInterval(interval.current);
    }; // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result, error]);

  return (
    <Grid container direction="column" alignItems="stretch" spacing={1}>
      <Grid item>
        <Typography variant="subtitle1" className={classes.BulletinHeader}>
          Met Office Forecast & Discussion
        </Typography>
      </Grid>
      <Grid item>
        <Card elevation={0} variant="outlined" className={classes.card}>
          <BulletinTabs
            selected={bulletinType}
            onSetBulletinType={setBulletinType}
          />
          <CardContent className={classes.cardContent}>
            {result && (
              <BulletinViewer bulletinType={bulletinType} bulletin={result} />
            )}
            {isLoading && (
              <Skeleton variant="rect" height="100%" width="100%" />
            )}
            {error && (
              <Alert severity="error">
                <AlertTitle>{error.message}</AlertTitle>
              </Alert>
            )}
          </CardContent>
        </Card>
      </Grid>
      <Grid item>
        <BulletinControls onOpenBulletinHistory={toggleBulletinHistory} />
      </Grid>
      <BulletinHistoryDialog
        open={bulletinHistoryOpen}
        toggleStatus={toggleBulletinHistory}
      />
    </Grid>
  );
};

export default BulletinComponent;
