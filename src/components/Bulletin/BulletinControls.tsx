/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Grid, Button, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  historyButton: {
    marginTop: '8px',
    marginRight: '32px',
  },
});

interface BulletinControlsProps {
  onOpenBulletinHistory?: () => void;
}

export const BulletinControls: React.FC<BulletinControlsProps> = ({
  onOpenBulletinHistory,
}: BulletinControlsProps) => {
  const classes = useStyles();
  return (
    <Grid container direction="row" justify="flex-end" alignItems="flex-end">
      <Grid item>
        <Button
          data-testid="history"
          size="small"
          variant="outlined"
          color="secondary"
          onClick={(): void => onOpenBulletinHistory()}
          className={classes.historyButton}
        >
          Forecast history list
        </Button>
      </Grid>
    </Grid>
  );
};
