/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  DialogProps,
  Typography,
  Dialog,
  DialogContent,
  DialogActions,
  makeStyles,
  Button,
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { NotificationTrigger } from '../NotificationTrigger/NotificationTrigger';

const useStyles = makeStyles({
  dialogHeader: {
    minHeight: '1em',
    boxShadow: '0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
    zIndex: 1,
  },
  closeButton: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  dialogTitle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogChildren: {
    margin: 'auto',
    padding: 20,
    minHeight: 200,
    minWidth: 400,
    width: 'inherit',
  },
});

interface ContentDialogProps extends DialogProps {
  open: boolean;
  toggleStatus: Function;
  title: string;
  children?: React.ReactNode;
  options?: React.ReactNode;
  onClose?: () => void;
}

export const ContentDialog: React.FC<ContentDialogProps> = ({
  open,
  toggleStatus,
  title,
  children,
  options,
  onClose,
  ...other
}: ContentDialogProps) => {
  const classes = useStyles();
  const handleClose = (): void => {
    if (onClose) {
      onClose();
    } else {
      toggleStatus();
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      maxWidth={other.maxWidth ? other.maxWidth : 'lg'}
      disableBackdropClick
      {...other}
    >
      <MuiDialogTitle disableTypography className={classes.dialogHeader}>
        <Button
          onClick={handleClose}
          className={classes.closeButton}
          color="secondary"
          size="medium"
          startIcon={<ArrowBackIosIcon fontSize="small" color="secondary" />}
          data-testid="contentdialog-close"
        >
          <Typography variant="subtitle1" color="secondary">
            BACK
          </Typography>
        </Button>
        <Typography
          data-testid="dialogTitle"
          variant="h6"
          className={classes.dialogTitle}
        >
          {title}
        </Typography>
      </MuiDialogTitle>
      <NotificationTrigger />
      <DialogContent className={classes.dialogChildren}>
        {children}
      </DialogContent>
      <DialogActions>{options}</DialogActions>
    </Dialog>
  );
};

export default ContentDialog;
