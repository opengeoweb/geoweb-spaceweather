/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';

import { render, getByText } from '@testing-library/react';
import Layout from './Layout';

describe('src/components/Layout/Layout', () => {
  it('shows correct components passed as props', () => {
    const TestComponent: React.FC<{ testId: string }> = ({
      testId,
    }: {
      testId: string;
    }) => <h1>{testId}</h1>;

    const props = {
      leftTop: <TestComponent testId="test-1" />,
      leftBottom: <TestComponent testId="test-2" />,
      right: <TestComponent testId="test-3" />,
    };
    const { container } = render(<Layout {...props} />);

    expect(getByText(container, 'test-1')).toBeTruthy();
    expect(getByText(container, 'test-2')).toBeTruthy();
    expect(getByText(container, 'test-3')).toBeTruthy();
  });
});
