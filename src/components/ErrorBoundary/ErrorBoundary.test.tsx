/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { render, getByTestId, fireEvent } from '@testing-library/react';
import { ErrorBoundary } from './ErrorBoundary';

const TestComponent: React.FC<{}> = () => {
  const [hasError, triggerError] = React.useState(false);

  if (hasError) {
    throw new Error('Triggered error');
  }
  return (
    <button
      type="button"
      data-testid="test"
      onClick={(): void => triggerError(true)}
    >
      Click to trigger error
    </button>
  );
};

describe('components/ErrorBoundary/ErrorBoundary', () => {
  it('should render with default props', () => {
    const { container } = render(
      <ErrorBoundary>
        <TestComponent />
      </ErrorBoundary>,
    );

    expect(getByTestId(container, 'test')).toBeTruthy();
  });

  it('should handle errors', () => {
    // mute errors in console
    const spy = jest.spyOn(console, 'error');
    spy.mockImplementation(() => null);

    const props = {
      onError: jest.fn(),
    };
    const { container, getByText } = render(
      <ErrorBoundary {...props}>
        <TestComponent />
      </ErrorBoundary>,
    );

    // trigger error
    fireEvent.click(getByTestId(container, 'test'));

    expect(getByText('Error: Triggered error')).toBeTruthy();
    expect(props.onError).toHaveBeenCalled();

    // restore mute errors in console
    spy.mockRestore();
  });
});
