/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { CircularProgress } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

interface GraphLoading {
  isLoading: boolean;
}

const GraphLoading: React.FC<GraphLoading> = ({ isLoading }: GraphLoading) => {
  if (!isLoading) {
    return null;
  }

  return (
    <CircularProgress
      style={{
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        left: 0,
        margin: 'auto',
      }}
      size={20}
      color="secondary"
    />
  );
};

interface GraphError {
  error: Error;
}
const GraphError: React.FC<GraphError> = ({ error }: GraphError) => {
  const [isVisible, setIsVisible] = React.useState(true);

  const hide = (): void => setIsVisible(false);

  React.useEffect(() => {
    const timer = setTimeout(hide, 3000);
    setIsVisible(true);

    return (): void => {
      clearTimeout(timer);
    };
  }, [error]);

  if (!error || !isVisible) {
    return null;
  }
  return (
    <Alert
      style={{
        position: 'absolute',
        right: 0,
        padding: '3px 8px',
      }}
      severity="error"
    >
      {error.message}
    </Alert>
  );
};

interface GraphStatus {
  isLoading: boolean;
  error: Error;
}
export const GraphStatus: React.FC<GraphStatus> = ({
  isLoading,
  error,
}: GraphStatus) => (
  <>
    <GraphLoading isLoading={isLoading} />
    <GraphError error={error} />
  </>
);

export default GraphStatus;
