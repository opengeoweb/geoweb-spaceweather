/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import { TimeSeries, Index } from 'pondjs';

export const createChartSeriesData = (
  response,
  { id, columns, timeWidth, graphType },
): [] => {
  return response.map((resp, responseIndex) => {
    const column = columns[responseIndex];
    const newColumns = Array.isArray(column)
      ? ['index', ...column]
      : ['index', column];

    // series
    const series = new TimeSeries({
      name: id,
      columns: newColumns,
      unit: resp.data.unit,
      points: resp.data.data.map(({ timestamp, value }) => {
        if (graphType === 'BAND') {
          return resp.data.parameter === 'bt'
            ? [
                // @ts-ignore // BandChart expects 4 values
                Index.getIndexString(timeWidth, new Date(timestamp)),
                [-value, value, -value, value], // Mirror Bt around x-axis
              ]
            : [
                // @ts-ignore // BandChart expects 4 values
                Index.getIndexString(timeWidth, new Date(timestamp)),
                [0, value, 0, value], // Other fields should not be mirrored
              ];
        }

        if (graphType === 'AREA') {
          return [
            // @ts-ignore
            Index.getIndexString(timeWidth, new Date(timestamp)),
            value,
            value,
          ];
        }

        return [
          // @ts-ignore
          Index.getIndexString(timeWidth, new Date(timestamp)),
          value,
        ];
      }),
    });

    return series;
  });
};
