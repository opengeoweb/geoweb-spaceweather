/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { Button } from '@material-ui/core';
import { TimeRange } from 'pondjs';
import UpdateIcon from '@material-ui/icons/Update';
import {
  getBeginTime,
  getEndTime,
  getDefaultTimeRange,
} from '../../utils/defaultTimeRange';

interface TimeSeriesFooter {
  timeRange: TimeRange;
  onResetTimeRange: (timeRange: TimeRange) => void;
}

const isSameTimeRange = (timeRange: TimeRange): boolean => {
  const beginTime = getBeginTime();
  const endTime = getEndTime();
  const [start, end] = timeRange.toJSON();
  const newStart = moment.utc(start);
  const newEnd = moment.utc(end);

  return (
    beginTime.diff(newStart, 'minutes') === 0 &&
    endTime.diff(newEnd, 'minutes') === 0
  );
};

export const TimeSeriesFooter: React.FC<TimeSeriesFooter> = ({
  timeRange,
  onResetTimeRange,
}: TimeSeriesFooter) => {
  const onClickReset = (): void => onResetTimeRange(getDefaultTimeRange());
  const isDisabled = isSameTimeRange(timeRange);

  return (
    <div
      style={{
        textAlign: 'right',
        position: 'absolute',
        right: 0,
        left: 0,
        top: 0,
        bottom: 0,
        zIndex: 10,
        pointerEvents: 'none',
      }}
    >
      <Button
        size="small"
        variant="outlined"
        color="secondary"
        onClick={onClickReset}
        disabled={isDisabled}
        style={{
          marginRight: 32,
          position: 'absolute',
          height: '40px',
          width: '58px',
          right: 0,
          top: 0,
          bottom: 0,
          margin: 'auto',
          pointerEvents: 'all',
          transform: 'translateY(-10px)',
          backgroundColor: '#fff',
        }}
      >
        <UpdateIcon />
      </Button>
    </div>
  );
};

export default TimeSeriesFooter;
