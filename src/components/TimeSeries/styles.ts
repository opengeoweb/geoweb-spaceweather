/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import { styler } from 'react-timeseries-charts';
import { GWTheme } from '@opengeoweb/theme';

export const curveColors = {
  kpIndex: '#417505',
  kpIndexForecast: '#8DB04E',
  primaryValue: 'gray',
  secondaryValue: 'red',
  lineSolarXray: 'violet',
  lineSolarWindDensity: 'red',
  lineSolarWindSpeed: 'orange',
  areaSolarWindSpeed: 'lightyellow',
  lineSolarWindPressure: 'blue',
  areaSolarWindPressure: 'lightblue',
};

export const style = styler([
  { key: 'kpIndex', color: curveColors.kpIndex, opacity: 0.75 },
  { key: 'kpIndexForecast', color: curveColors.kpIndexForecast, opacity: 0.75 },
  {
    key: 'primaryValue',
    color: curveColors.primaryValue,
    width: 1,
    opacity: 0.5,
  },
  {
    key: 'secondaryValue',
    color: curveColors.secondaryValue,
    width: 1,
    opacity: 0.5,
  },
  {
    key: 'lineSolarXray',
    color: curveColors.lineSolarXray,
    width: 2,
    opacity: 0.5,
  },
  {
    key: 'lineSolarWindDensity',
    color: curveColors.lineSolarWindDensity,
    width: 2,
    opacity: 0.5,
  },
  {
    key: 'lineSolarWindSpeed',
    color: curveColors.lineSolarWindSpeed,
    width: 2,
    opacity: 0.5,
  },
  { key: 'areaSolarWindSpeed', color: curveColors.areaSolarWindSpeed },
  {
    key: 'lineSolarWindPressure',
    color: curveColors.lineSolarWindPressure,
    width: 2,
    opacity: 0.5,
  },
  { key: 'areaSolarWindPressure', color: curveColors.areaSolarWindPressure },
]);

const chartTitleStyle = (): React.CSSProperties => ({
  fill: '#000000',
  fontFamily: GWTheme.typography.fontFamily,
});
export const titleStyle = chartTitleStyle();

export const chartThresholdStyle = (
  color?: string,
): { line: React.CSSProperties; label: React.CSSProperties } => ({
  line: {
    stroke: color || '#000000',
    strokeWidth: 1,
    opacity: 0.5,
    strokeDasharray: '5,3',
  },
  label: {
    fill: '#000000',
    opacity: 0.6,
    fontFamily: GWTheme.typography.fontFamily,
  },
});
export const thresholdStyle = chartThresholdStyle();

const chartBaselineStyle = (): { line: React.CSSProperties } => ({
  line: {
    stroke: '#000000',
    strokeDasharray: 'none',
    strokeWidth: 1,
    opacity: 1,
  },
});
export const baselineStyle = chartBaselineStyle();

const chartTitleBoxStyle = (): React.CSSProperties => ({
  opacity: 0,
});
export const titleBoxStyle = chartTitleBoxStyle();

export const valueStyle = (color: string): React.CSSProperties => ({
  fontFamily: GWTheme.typography.fontFamily,
  fontSize: '14px',
  color,
  opacity: 0.8,
});

const chartYAxisStyle = (): {
  values: React.CSSProperties;
  ticks: React.CSSProperties;
  axis: React.CSSProperties;
  label: React.CSSProperties;
} => ({
  label: {
    fill: '#000000',
  },
  values: {
    fill: '#000000',
  },
  ticks: { stroke: '#000000', opacity: 0.6 },
  axis: { stroke: '#000000' },
});
export const yAxisStyle = chartYAxisStyle();

export const chartMarkerStyle = (color = '#AAA'): React.CSSProperties => ({
  color,
  marginLeft: '5px',
  fontSize: '13px',
  fontFamily: GWTheme.typography.fontFamily,
});
export const markerStyle = chartMarkerStyle();

export const trackerStyle = {
  line: {
    stroke: '#0075a9',
    strokeWidth: 1.5,
  },
};
