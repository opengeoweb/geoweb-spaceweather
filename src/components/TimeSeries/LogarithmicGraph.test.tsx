/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import LogarithmicGraph from './LogarithmicGraph';
import { logGraphWithSeries } from '../../utils/DummyData';
import { TimeTrackerProvider } from './TimeTrackerContext';
import { GraphContainer } from './GraphContainer';
import { defaultTimeRange } from '../../utils/defaultTimeRange';

describe('src/components/TimeSeries/LogarithmicGraph', () => {
  // Need to set the offsetWidth for Resizable to render it's children
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  });

  it('should show the graph title', () => {
    const { getByText } = render(
      <TimeTrackerProvider>
        <GraphContainer timeRange={defaultTimeRange}>
          {LogarithmicGraph(logGraphWithSeries)}
        </GraphContainer>
      </TimeTrackerProvider>,
    );
    expect(getByText(logGraphWithSeries.title)).toBeTruthy();
  });

  it('should show the given y axis range', () => {
    const { container } = render(
      <TimeTrackerProvider>
        <GraphContainer timeRange={defaultTimeRange}>
          {LogarithmicGraph(logGraphWithSeries)}
        </GraphContainer>
      </TimeTrackerProvider>,
    );
    const yAxisValues = container.querySelectorAll('.yaxis .tick');
    // 100m corresponds to 0.1 (yMinValue) and it is hardcoded due to an issue retrieving the correct value from yAxisValues
    expect(yAxisValues[0].textContent).toEqual('100m');
    // 1.0k corresponds to 1000 (yMaxValue) and it is hardcoded due to an issue retrieving the correct value from yAxisValues
    expect(yAxisValues[yAxisValues.length - 1].textContent).toEqual('1.0k');
  });
});
