/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */
import { TimeSeries } from 'pondjs';
import { renderHook, act } from '@testing-library/react-hooks';
import {
  stateAsArray,
  createStateById,
  hasSeries,
  GraphState,
  cancelRequests,
  getGraphIndexWithLastData,
  createStreamDict,
  usePrevious,
  useUserSleeping,
} from './TimeSeries.utils';
import { GraphItem } from './types';
import { TimeseriesParams, StreamResponse } from '../../types';
import * as apiUtils from '../../utils/api';

describe('src/components/TimeSeries/TimeSeries.utils', () => {
  describe('createStateById', () => {
    it('should create dict state', () => {
      const charts = [
        { id: 'chart 1', params: [] },
        { id: 'chart 2', params: [] },
      ] as GraphItem[];

      const params = [
        {
          // eslint-disable-next-line @typescript-eslint/camelcase
          time_stop: new Date().toUTCString(),
          // eslint-disable-next-line @typescript-eslint/camelcase
          time_end: new Date().toUTCString(),
        },
      ] as TimeseriesParams;

      expect(createStateById(charts, params)).toEqual({
        'chart 1': {
          isLoading: true,
          error: null,
          data: [
            {
              ...charts[0],
              series: [],
            },
          ],
        },
        'chart 2': {
          isLoading: true,
          error: null,
          data: [
            {
              ...charts[1],
              series: [],
            },
          ],
        },
      });
    });
  });

  describe('stateAsArray', () => {
    it('should create array of state object', () => {
      const charts = {
        'chart-1': {
          title: 'chart 1',
        },
        'chart-2': {
          title: 'chart 2',
        },
      };

      expect(stateAsArray(charts)).toEqual([
        { title: 'chart 1' },
        { title: 'chart 2' },
      ]);
    });
  });

  describe('hasSeries', () => {
    it('should return if graph has series', () => {
      expect(hasSeries({} as GraphState)).toBeFalsy();
      expect(hasSeries({ data: [] } as GraphState)).toBeFalsy();
      expect(hasSeries({ data: [{ series: [] }] } as GraphState)).toBeFalsy();
      expect(
        hasSeries({ data: [{ series: [new TimeSeries()] }] } as GraphState),
      ).toBeTruthy();
    });
  });

  describe('cancelRequests', () => {
    it('should cancel requests', () => {
      const charts = [
        { params: [{ stream: 'test-s-1', parameter: 'test-p-1' }] },
        { params: [{ stream: 'test-s-2', parameter: 'test-p-2' }] },
      ] as TimeseriesParams;
      const spy = jest.spyOn(apiUtils, 'cancelRequestById');

      cancelRequests(charts as GraphItem[]);

      expect(spy).toHaveBeenCalledWith(
        apiUtils.createCancelRequestId(charts[0].params[0]),
      );
      expect(spy).toHaveBeenCalledWith(
        apiUtils.createCancelRequestId(charts[1].params[0]),
      );
    });
  });

  describe('getGraphIndexWithLastData', () => {
    it('should get index of last graph with data', () => {
      const chartsWithSeries = [
        { data: [{ series: [new TimeSeries()] }] },
        { data: [{ series: [new TimeSeries()] }] },
        { data: [{ series: [] }] },
      ] as GraphState[];
      expect(getGraphIndexWithLastData(chartsWithSeries)).toEqual(1);

      const chartsWithouSeries = [{ data: [{ series: [] }] }] as GraphState[];
      expect(getGraphIndexWithLastData(chartsWithouSeries)).toEqual(-1);
    });
  });

  describe('createStreamDict', () => {
    it('should create dict object from streams', () => {
      const streams = [
        { start: '', stream: 'xray' },
        { start: '', stream: 'kp' },
      ] as StreamResponse[];

      expect(createStreamDict(streams)).toEqual({
        xray: streams[0],
        kp: streams[1],
      });
    });
  });

  describe('usePrevious', () => {
    it('should return previous value', () => {
      const testValue = 1;
      const component = renderHook(() => usePrevious(testValue));
      const { result, rerender } = component;
      expect(result.current).toBeUndefined();
      rerender(component);
      expect(result.current).toEqual(testValue);
    });
  });

  describe('useUserSleeping', () => {
    it('should let user fall asleep', () => {
      jest.useFakeTimers();
      const component = renderHook(() => useUserSleeping());
      const { result, rerender } = component;

      expect(result[0]).toBeFalsy();

      act(() => {
        jest.runAllTimers();
        rerender(component);
      });

      const [isSleeping, resetIsSleeping] = result.current;

      expect(isSleeping).toBeTruthy();

      act(() => {
        resetIsSleeping();
      });
      expect(result.current[0]).toBeFalsy();
    });
  });
});
