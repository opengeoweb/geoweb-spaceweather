/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  ChartRow,
  Charts,
  Baseline,
  YAxis,
  LineChart,
} from 'react-timeseries-charts';
import {
  titleStyle,
  titleBoxStyle,
  yAxisStyle,
  baselineStyle,
  thresholdStyle,
  style,
} from './styles';
import { GraphItem } from './types';
import { getGraphHeightInPx } from './TimeSeries.utils';

const graphHeight = getGraphHeightInPx();

export const LogarithmicGraph: React.FC<GraphItem> = ({
  id,
  title,
  yMinValue,
  yMaxValue,
  columns,
  series,
  threshold = [],
}: GraphItem) => {
  return (
    <ChartRow
      title={title}
      titleStyle={titleStyle}
      titleBoxStyle={titleBoxStyle}
      height={graphHeight}
      key={id}
    >
      <YAxis
        id="y"
        min={yMinValue}
        max={yMaxValue}
        format=",.0f"
        type="log"
        width={40}
        style={yAxisStyle}
      />
      {series.map((serie, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Charts key={`-${id}-charts-${index}`}>
          <LineChart
            axis="y"
            // eslint-disable-next-line react/no-array-index-key
            key={`line-${index}`}
            breakLine
            series={serie}
            columns={columns}
            style={style}
            interpolation="curveLinear"
          />
          <Baseline
            axis="y"
            key="baseline"
            style={baselineStyle}
            value={yMinValue}
            position="right"
          />
          {threshold &&
            threshold.map(line => {
              return (
                <Baseline
                  axis="y"
                  key={`threshold-${line}`}
                  style={thresholdStyle}
                  value={line.value}
                  label={line.title}
                  position="right"
                />
              );
            })}
        </Charts>
      ))}
    </ChartRow>
  );
};

export default LogarithmicGraph;
